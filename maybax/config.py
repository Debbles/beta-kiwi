"""Basic implementation for Configs"""
from __future__ import annotations

import abc
import collections.abc as collections_abc
import logging
import os
import typing

import attr
import hikari

__all__: list[str] = ["FullConfig", "DatabaseConfig", "Tokens"]

logger = logging.getLogger(__name__)
logger.setLevel("INFO")
logger.handlers = [
    logging.StreamHandler(),
]


ConfigT = typing.TypeVar("ConfigT", bound="Config")


class Config(abc.ABC):
    """Provides an implementation for the Basic Configuration."""

    __slots__ = ()

    @classmethod
    @abc.abstractmethod
    def from_environ(cls: type[ConfigT]) -> ConfigT:
        """Checks local environment variables and attempts to build the Config."""
        raise NotImplementedError


@attr.s(repr=False)
class DatabaseConfig(Config):
    """Provides an implementation for the Database Configuration."""

    host: str = attr.ib(default="localhost")
    username: str = attr.ib(default="postgres")
    password: str = attr.ib(default="postgres")
    name: str = attr.ib(default="postgres")
    port: int = attr.ib(default=5432)

    @classmethod
    def from_environ(cls) -> DatabaseConfig:
        host = os.environ.get("PG_HOST", "localhost")
        username = os.environ.get("PG_USER", "postgres")
        password = os.environ.get("PG_PASS", "postgres")
        name = os.environ.get("PG_NAME", "postgres")
        if port := os.environ.get("PG_PORT", "").isdigit():
            port = int(port)
        else:
            port = 5432
        logger.info(
            f"Starting Maybax with Postgres Config (Host: {host}, Username: {username}, Name: {name}, Port, {port})"
        )
        return cls(
            host=host,
            username=username,
            password=password,
            name=name,
            port=port,
        )


@attr.s(repr=False)
class Tokens(Config):
    """Provides an implementation for the Token Configuration."""

    discord_bot: str = attr.ib()
    marvel_public: str | None = attr.ib(default=None)
    marvel_private: str | None = attr.ib(default=None)
    merriam_webster: str | None = attr.ib(default=None)

    @classmethod
    def from_environ(cls) -> Tokens:
        discord_token = os.environ.get("BOT_TOKEN")
        if not discord_token:
            raise RuntimeError("Must provide discord bot token in environment variable BOT_TOKEN.")

        if marvel_pub := os.environ.get("MARVEL_PUB", None):
            logger.info(f"Marvel Public key found: {marvel_pub}")
            if marvel_private := os.environ.get("MARVEL_PRIV", None):
                logger.info("Marvel Private key found.")
        else:
            logger.info("No Marvel Public Key Found. Marvel API will be inaccessible.")
            marvel_private = None

        if merriam_webster := os.environ.get("MERWEB_KEY", None):
            logger.info("Merriam Webster key found.")

        return cls(
            discord_bot=discord_token,
            marvel_public=marvel_pub,
            marvel_private=marvel_private,
            merriam_webster=merriam_webster,
        )


DEFAULT_CACHE = hikari.CacheComponents.GUILDS | hikari.CacheComponents.GUILD_CHANNELS | hikari.CacheComponents.ROLES


@attr.s
class NewRelicConfig(Config):
    """Provides a class and methods to handle New Relic logging tokens."""

    app_id: int = attr.ib()
    app_name: str = attr.ib()
    license_key: str = attr.ib()
    region: str = attr.ib()
    log_level: int | str | dict[str, typing.Any] | None = attr.ib(default=logging.INFO)

    @classmethod
    def from_environ(cls) -> NewRelicConfig | None:
        log_level = os.environ.get("log_level", "INFO")
        if log_level.isdigit():
            log_level = int(log_level)
        elif log_level.lower() not in [
            "notset",
            "debug",
            "info",
            "warn",
            "error",
            "critical",
        ]:
            raise ValueError("Invalid log level found in config")

        nr_app_id = os.environ.get("NEW_RELIC_APP_ID", None)
        if nr_app_id is None:
            logging.info("NEW_RELIC_APP_ID not provided as env variable. Skipping NewRelic Logging Setup.")
            return
        elif not nr_app_id.isdigit():
            logging.info("NEW_RELIC_APP_ID appears to be invalid! Please provide a valid App ID!")
            return
        else:
            nr_app_id = int(nr_app_id)
        nr_app_name = os.environ.get("NEW_RELIC_APP_NAME", None)
        if not nr_app_name:
            logging.info("NEW_RELIC_APP_NAME not provided as env variable. Skipping NewRelic Logging Setup.")
            return
        nr_license_key = os.environ.get("NEW_RELIC_LICENSE_KEY", None)
        if not nr_license_key:
            logging.info("NEW_RELIC_LICENSE_KEY not provided as env variable. Skipping NewRelic Logging Setup.")
            return
        nr_region = os.environ.get("NEW_RELIC_REGION", None)
        if not nr_region:
            logging.info("NEW_RELIC_REGION not provided as env variable. Skipping NewRelic Logging Setup.")
            return

        import newrelic_logger  # noqa: F401

        return cls(
            app_id=nr_app_id, app_name=nr_app_name, license_key=nr_license_key, region=nr_region, log_level=log_level
        )


@attr.s(repr=False)
class FullConfig(Config):  # pylint: disable=R0902
    """Provides a class and methods to handle Maybax2's Configuration."""

    database: DatabaseConfig = attr.ib()
    tokens: Tokens = attr.ib()
    cache: hikari.CacheComponents = attr.ib(default=DEFAULT_CACHE)
    intents: hikari.Intents = attr.ib(default=hikari.Intents.ALL_UNPRIVILEGED)
    log_level: int | str | dict[str, typing.Any] | None = attr.ib(default=logging.INFO)
    mention_prefix: bool = attr.ib(default=True)
    owner_only: bool = attr.ib(default=False)
    prefixes: collections_abc.Set[str] = attr.ib(default=frozenset("m;"))
    set_global_commands: int | hikari.Snowflake | None = attr.ib(default=None)
    new_relic: NewRelicConfig | None = attr.ib(default=None)

    @classmethod
    def from_environ(cls) -> FullConfig:
        logger.info("### Setting up Config now!")
        log_level = os.environ.get("LOG_LEVEL", "INFO")
        if log_level.isdigit():
            log_level = int(log_level)
        elif log_level.lower() not in [
            "notset",
            "debug",
            "info",
            "warn",
            "error",
            "critical",
        ]:
            raise ValueError("Invalid log level found in config")
        logger.info(f"Set logger to {log_level}.")

        set_global_commands = os.environ.get("set_global_commands", None)
        if set_global_commands:
            logger.info("Commands marked for Local only. Trying to get Guild.")
            set_global_commands = hikari.Snowflake(set_global_commands)
            logger.info("Snowflake Converted.")
        else:
            set_global_commands = True
            logger.info("Marking all commands as Global.")

        logger.info("Set global_commands: {set_global_commands}.")

        intents = os.environ.get("DISCORD_INTENTS", hikari.Intents.ALL_UNPRIVILEGED)
        logger.info("Setting Discord Intents: {intents}.")

        mention_prefix = bool(os.environ.get("MENTION_PREFIX", True))
        logger.info(f"Setting member prefix: {mention_prefix}.")

        prefixes = set(os.environ.get("PREFIXES", "m;").split(","))
        logger.info(f"Setting bot prefixes: {prefixes}.")

        owner_only = bool(os.environ.get("OWNER_ONLY", False))
        logger.info(f"Checking for Owner Only Mode: {owner_only}.")

        return cls(
            database=DatabaseConfig.from_environ(),
            tokens=Tokens.from_environ(),
            cache=DEFAULT_CACHE,
            intents=typing.cast(hikari.Intents, intents),
            log_level=log_level,
            mention_prefix=mention_prefix,
            owner_only=owner_only,
            prefixes=prefixes,
            set_global_commands=set_global_commands,
            new_relic=NewRelicConfig.from_environ(),
        )
