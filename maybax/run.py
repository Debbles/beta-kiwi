"""Runs the Maybax2 bot."""
import logging
import os

from client import build_bot

logger = logging.getLogger(__name__)

try:
    import debugpy

    debugpy.listen(("0.0.0.0", 5678))  # noqa: S104
    # print("Waiting for client to attach...")
    # debugpy.wait_for_client()
except ImportError:
    logger.info("No DebugPy; skipping debugger setup")

if os.name != "nt":
    logger.info("Using UV Loop")
    import uvloop

    uvloop.install()

if __name__ == "__main__":
    build_bot().run()
