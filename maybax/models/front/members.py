""""Provides 3 models to represent fronting members.

PluralKitMember
TupperMember
PatchworkMember"""
from __future__ import annotations

import datetime
import logging
from typing import TypedDict

import aiopg
import aioredis
import attr
import hikari
import psycopg2.extras
import pytz
from psycopg2 import Error
from tanjun.abc import SlashContext
from util.redis_utils import (
    asdict_export_serializer,
    attrs_asdict_to_redis_serializer,
    attrs_to_postgres_serializer,
)

M_ID = 227117043143540736
DEFAULT_PATCHWORK_PFP = "https://cdn.discordapp.com/attachments/838613230536359997/884605473167384626/logo128.png"

logger = logging.getLogger(__name__)


class _ProxyTags(TypedDict):
    """A helper class for typing PluralKitMember."""

    prefix: str | None
    suffix: str | None


@attr.s
class PluralKitMember:  # pylint: disable=R0902
    """A simple model to represent a PluralKitMember for fronting."""

    name: str = attr.ib()
    created: str = attr.ib()
    id_: int | None = attr.ib()
    message_count: int = attr.ib(default=9999)
    keep_proxy: bool = attr.ib(default=False)
    pronouns: str | None = attr.ib(default=None)
    color: str | None = attr.ib(default=None)
    display_name: str | None = attr.ib(default=None)
    avatar_url: str = attr.ib(default=DEFAULT_PATCHWORK_PFP)
    birthday: str | None = attr.ib(default=None)
    description: str | None = attr.ib(default=None)
    banner: str | None = attr.ib(default=None)
    privacy: str = attr.ib(default="public")
    visibility: str = attr.ib(default="public")
    name_privacy: str = attr.ib(default="public")
    description_privacy: str = attr.ib(default="public")
    birthday_privacy: str = attr.ib(default="public")
    pronoun_privacy: str = attr.ib(default="public")
    avatar_privacy: str = attr.ib(default="public")
    metadata_privacy: str = attr.ib(default="public")
    proxy_tags: list[_ProxyTags] = attr.ib(factory=list[_ProxyTags])

    def to_dict(self) -> dict:
        """Returns this instance as a dictionary."""
        return attr.asdict(self, value_serializer=asdict_export_serializer)


@attr.s
class TupperMember:  # pylint: disable=R0902
    """A simple model to represent a TupperMember for fronting."""

    name: str = attr.ib()
    id_: int | None = attr.ib()
    position: int | None = attr.ib()
    tag: str | None = attr.ib()
    created_at: str | None = attr.ib()
    posts: int = attr.ib(default=99999)
    show_brackets: bool = attr.ib(default=False)
    user_id: str = attr.ib(default="227117043143540736")
    avatar_url: str = attr.ib(default=DEFAULT_PATCHWORK_PFP)
    birthday: str | None = attr.ib(default=None)
    description: str | None = attr.ib(default=None)
    nick: str | None = attr.ib(default=None)
    group_id: int | None = attr.ib(default=204869)
    group_pos: int | None = attr.ib(default=0)
    brackets: list[str] = attr.ib(factory=list)

    def to_dict(self) -> dict:
        """Returns this instance as a dictionary."""
        return attr.asdict(self, value_serializer=asdict_export_serializer)


@attr.s
class PatchworkMember:  # pylint: disable=R0902
    """A simple model to represent a PatchworkMember for fronting."""

    name: str = attr.ib()
    id_: int | None = attr.ib(default=None)
    created_at: datetime.datetime = attr.ib(
        factory=lambda: datetime.datetime.now().astimezone(pytz.timezone("US/Eastern"))
    )
    tag: str | None = attr.ib(default=None)
    display_name: str | None = attr.ib(default=None)
    indicators: list[_ProxyTags] = attr.ib(factory=list[_ProxyTags])
    pronouns: str | None = attr.ib(default=None)
    color: str | None = attr.ib(default=None)
    avatar_url: str = attr.ib(default=DEFAULT_PATCHWORK_PFP)
    birthday: str | None = attr.ib(default=None)
    description: str | None = attr.ib(default=None)
    tupper_id: int | None = attr.ib(default=None)
    pluralkit_id: str | None = attr.ib(default=None)
    position: int | None = attr.ib(default=0)
    tupper_group_id: int | None = attr.ib(default=204869)
    tupper_group_pos: int | None = attr.ib(default=0)
    groups_id: int | None = attr.ib(default=None)
    system_id: int | None = attr.ib(default=None)

    def to_dict(self) -> dict:
        """Return this instance as a dictionary."""
        return attr.asdict(self, value_serializer=asdict_export_serializer)

    def update_from_pluralkit(self, part: PluralKitMember) -> None:
        """Update this instance from a PluralKitMember."""
        self.name = part.name
        self.pluralkit_id = part.id_
        self.created_at = datetime.datetime.fromisoformat(part.created[:-1])
        if part.avatar_url:
            self.avatar_url = part.avatar_url
        if part.birthday:
            self.birthday = part.birthday
        if part.description:
            self.description = part.description
        if part.color:
            self.color = part.color
        if part.pronouns:
            self.pronouns = part.pronouns
        self.indicators = part.proxy_tags

    def update_from_tupperbox(self, part: TupperMember) -> None:
        """Update this instance from a TupperMember."""
        self.tupper_id = part.id_
        if part.avatar_url:
            self.avatar_url = part.avatar_url
        self.name = part.name
        self.position = part.position
        if part.created_at:
            self.created_at = datetime.datetime.fromisoformat(part.created_at[:-1])
        if part.birthday:
            self.birthday = part.birthday
        if part.description:
            self.description = part.description
        if part.tag:
            self.tag = part.tag
        self.tupper_group_id = part.group_id
        self.tupper_group_pos = part.group_pos
        for tag in part.brackets:
            included = False
            for indicator in self.indicators:
                if tag == indicator["prefix"] and tag == indicator["suffix"]:
                    included = True
                    break
            if not included:
                self.indicators.append({"prefix": tag.strip(), "suffix": tag.strip()})

    def to_tupper(self) -> TupperMember:
        """Convert this instance to a TupperMember."""
        tags = []
        for ind in self.indicators:
            if ind.get("prefix"):
                tags.append(ind["prefix"])
            if ind.get("suffix"):
                tags.append(ind["suffix"])
        return TupperMember(
            id_=self.tupper_id,
            name=self.name,
            position=9,
            avatar_url=self.avatar_url,
            birthday=self.birthday,
            tag=" | Patchwork Collective",
            description=self.description,
            group_id=self.tupper_group_id,
            group_pos=self.tupper_group_pos,
            created_at=self.created_at.isoformat(),
            brackets=tags,
        )

    @classmethod
    def from_tupper(cls, tupper: TupperMember) -> PatchworkMember:
        """"""
        new_indc = []
        for tag in tupper.brackets:
            if tag == "":
                continue
            new_indc.append({"prefix": tag, "suffix": tag})

        return cls(
            name=tupper.name,
            tupper_id=tupper.id_,
            position=tupper.position,
            created_at=datetime.datetime.fromisoformat(tupper.created_at[:-1])
            if tupper.created_at
            else datetime.datetime.now().astimezone(pytz.timezone("US/Eastern")),
            avatar_url=tupper.avatar_url,
            birthday=tupper.birthday if tupper.birthday else "",
            description=tupper.description if tupper.description else "",
            tag=tupper.tag if tupper.tag else "",
            tupper_group_id=tupper.group_id,
            tupper_group_pos=tupper.group_pos,
            display_name=tupper.name,
            indicators=new_indc,
        )

    def to_pluralkit(self) -> PluralKitMember:
        """Convert this instance to a PluralKitMember."""
        tags = []
        for ind in self.indicators:
            new_ind = {"suffix": "", "prefix": ""}
            if ind.get("suffix"):
                new_ind["suffix"] = ind
            if ind.get("prefix"):
                new_ind["prefix"] = ind
            tags.append(new_ind)
        return PluralKitMember(
            id_=self.pluralkit_id, name=self.name, created=self.created_at.isoformat(), proxy_tags=tags
        )

    @classmethod
    def from_pluralkit(cls, pk_part: PluralKitMember) -> PatchworkMember:
        return cls(
            pluralkit_id=pk_part.id_,
            name=pk_part.name,
            created_at=datetime.datetime.fromisoformat(pk_part.created[:-1]),
            display_name=pk_part.display_name if pk_part.display_name else "",
            tag="",
            pronouns=pk_part.pronouns,
            color=pk_part.color,
            avatar_url=pk_part.avatar_url,
            birthday=pk_part.birthday if pk_part.birthday else "",
            description=pk_part.description if pk_part.description else "",
            indicators=pk_part.proxy_tags,
        )

    @classmethod
    async def from_cache(cls, member_id: int, part_id: int, redis: aioredis.Redis) -> PatchworkMember | None:
        redis_key_member = f"{member_id}:{part_id}:member"
        if await redis.exists(redis_key_member):
            member_data = await redis.hgetall(redis_key_member)
            new_ind = []
            if member_data.get("indicators"):
                for pre_suf in member_data["indicators"].split("|"):
                    pre_suf_list = pre_suf.split(",")
                    ind_entry = {"prefix": "`[None]`", "suffix": "`[None]`"}
                    for entry in pre_suf_list:
                        entry = entry.split(";")
                        if entry[0] == "prefix" and len(entry) > 1:
                            if entry[1].lower() not in ["none", "[none]"]:
                                ind_entry["prefix"] = entry[1].strip()
                        elif entry[0] == "suffix" and len(entry) > 1:
                            if entry[1].lower() not in ["none", "[none]"]:
                                ind_entry["suffix"] = entry[1].strip()
                    new_ind.append(ind_entry)
            member_data["indicators"] = new_ind
            try:
                member_data["created_at"] = datetime.datetime.fromisoformat(member_data["created_at"])
            except Exception:
                member_data["created_at"] = datetime.datetime.fromtimestamp(float(member_data["created_at"]))
            return cls(**member_data)
        logger.info(f"Member for {redis_key_member} not found.")
        return None

    async def cache(self, member_id: int, redis: aioredis.Redis):
        logger.info(f"Caching Member {self.name}. Starting!")
        redis_key_set = f"{member_id}:member_list"
        redis_key_member = f"{member_id}:{self.id_}:member"
        member_data = attr.asdict(self, value_serializer=attrs_asdict_to_redis_serializer)
        # await redis.delete(redis_key_member)
        # await redis.hset(redis_key_member, mapping=member_data)
        await redis.hset(redis_key_member, key="id_", value=member_data["id_"])
        await redis.hset(redis_key_member, key="name", value=member_data["name"])
        await redis.hset(redis_key_member, key="created_at", value=member_data["created_at"])
        await redis.hset(redis_key_member, key="tag", value=member_data["tag"])
        await redis.hset(redis_key_member, key="display_name", value=member_data["display_name"])
        await redis.hset(redis_key_member, key="indicators", value=member_data["indicators"])
        await redis.hset(redis_key_member, key="pronouns", value=member_data["pronouns"])
        await redis.hset(redis_key_member, key="color", value=member_data["color"])
        await redis.hset(redis_key_member, key="avatar_url", value=member_data["avatar_url"])
        if isinstance(member_data["birthday"], datetime.date) or isinstance(member_data["birthday"], datetime.datetime):
            member_data["birthday"] = member_data["birthday"].isoformat()
        await redis.hset(redis_key_member, key="birthday", value=member_data["birthday"])
        await redis.hset(redis_key_member, key="description", value=member_data["description"])
        await redis.hset(redis_key_member, key="tupper_id", value=member_data["tupper_id"])
        await redis.hset(redis_key_member, key="pluralkit_id", value=member_data["pluralkit_id"])
        await redis.hset(redis_key_member, key="position", value=member_data["position"])
        await redis.hset(redis_key_member, key="tupper_group_id", value=member_data["tupper_group_id"])
        await redis.hset(redis_key_member, key="tupper_group_pos", value=member_data["tupper_group_pos"])
        await redis.hset(redis_key_member, key="groups_id", value=member_data["groups_id"])
        await redis.hset(redis_key_member, key="system_id", value=member_data["system_id"])
        await redis.sadd(redis_key_set, member_data["id_"])
        logger.info(f"Caching Member {self.name}. Complete!")

    async def save_to_postgres(self, system_id: int, pg_pool: aiopg.Pool) -> PatchworkMember:
        logger.debug(f"Saving Member {self.name} To Postgres Starting!")
        with await pg_pool.cursor() as cursor:
            ins_data = attr.asdict(self, value_serializer=attrs_to_postgres_serializer,) | {
                "system_id": system_id,
            }
            ins_data["tupper_id"] = str(self.tupper_id) if self.tupper_id else None
            ins_data["pluralkit_id"] = str(self.pluralkit_id) if self.pluralkit_id else None
            try:
                await cursor.execute(
                    """INSERT INTO system_member
                    (name, created_at, tag, display_name, indicators, pronouns,
                    color, avatar_url, birthday, description, tupper_id, pluralkit_id,
                    position, tupper_group_id, tupper_group_position, system_id)
                    VALUES
                    (%(name)s, %(created_at)s, %(tag)s, %(display_name)s,
                    %(indicators)s, %(pronouns)s, %(color)s, %(avatar_url)s,
                    %(birthday)s, %(description)s, %(tupper_id)s, %(pluralkit_id)s,
                    %(position)s, %(tupper_group_id)s, %(tupper_group_pos)s, %(system_id)s)
                    RETURNING id;""",
                    ins_data,
                )
            except Error:
                logger.info("Attempting to handling Unique violation exception now----")
                await cursor.execute(
                    """UPDATE system_member SET
                    name=%(name)s, created_at=%(created_at)s, tag=%(tag)s,
                    display_name=%(display_name)s, indicators=%(indicators)s,
                    pronouns=%(pronouns)s, color=%(color)s, avatar_url=%(avatar_url)s,
                    birthday=%(birthday)s, description=%(description)s,
                    tupper_id=%(tupper_id)s, pluralkit_id=%(pluralkit_id)s,
                    position=%(position)s, tupper_group_id=%(tupper_group_id)s,
                    tupper_group_position=%(tupper_group_pos)s, system_id=%(system_id)s
                    WHERE id=%(id_)s
                    RETURNING id;""",
                    ins_data,
                )

            id_ = (await cursor.fetchone())[0]
            self.id_ = id_
        logger.debug(f"Saving Member {self.name} To Postgres Complete Successful!")
        return self

    @classmethod
    async def from_postgres(cls, member_id: int, pg_pool: aiopg.Pool, redis: aioredis.Redis):
        """Not finished"""
        logger.debug(f"Fetching Member {member_id} from Postgres starting!")
        with await pg_pool.cursor(cursor_factory=psycopg2.extras.RealDictCursor) as cursor:
            await cursor.execute(
                """SELECT id, name, created, tag, display_name, indicators, pronouns,
                color, avatar_url, birthday, description, tupper_id, pluralkit_id,
                position, tupper_group_id, tupper_group_pos, color
                FROM system_member
                WHERE id = %(member_id)s;""",
                {"member_id": member_id},
            )
            member_data = await cursor.fetchone()
        logger.debug(f"Fetching Member {member_id} from Postgres Complete!")
        return cls(**member_data)

    @staticmethod
    def embed(member: PatchworkMember, /, ctx: SlashContext, show: bool = False) -> hikari.Embed:
        embed = (
            hikari.Embed(
                title=member.name,
                description=member.description if member.description else "-",
                color=member.color if member.color else "000000",
            )
            .add_field(
                name="Created At:",
                value=f"{member.created_at if member.created_at else '-'}",
                inline=True,
            )
            .add_field(name="Birthday:", value=member.birthday if member.birthday else "-", inline=True)
            .add_field(name="Pronouns:", value=member.pronouns if member.pronouns else "-", inline=True)
            .set_thumbnail(member.avatar_url)
            .set_author(name=ctx.member.username, icon=ctx.member.avatar_url)
        )
        indicators = []
        for indc in member.indicators:
            if indc["prefix"]:
                indc["prefix"] = indc["prefix"].strip()
            else:
                indc["prefix"] = "`[ None ]`"
            if indc["suffix"]:
                indc["suffix"] = indc["suffix"].strip()
            else:
                indc["suffix"] = "`[ None ]`"
            indicators.append(f"Prefix: {indc['prefix']}\nSuffix: {indc['suffix']}")
        if not indicators:
            indicators = ["`[ None ]`", ""]
        if not show:
            embed.set_footer(
                text=(
                    f"ID: {member.id_} | "
                    f"PK ID: {member.pluralkit_id if member.pluralkit_id else '-'} |  "
                    f"Tupper ID: {member.tupper_id if member.tupper_id else '-'}"
                )
            ).add_field(
                name="Indicators:",
                value="\n".join(indicators),
                inline=True,
            )
        return embed
