"""Provides 3 models to work with data.

PluralKitGroup

TupperGroup

PatchworkGroup"""
from __future__ import annotations

import datetime
import logging

import aiopg
import aioredis
import attr
import hikari
import pytz
from psycopg2 import IntegrityError
from tanjun.abc import SlashContext
from util.redis_utils import (
    attrs_asdict_to_redis_serializer,
    attrs_to_postgres_serializer,
)

M_ID = 227117043143540736
DEFAULT_PATCHWORK_PFP = "https://cdn.discordapp.com/attachments/838613230536359997/884605473167384626/logo128.png"


logger = logging.getLogger(__name__)


@attr.s
class PluralKitGroup:
    """Provides a simple model to represent a PluralKitGroup fronting group."""

    id_: str = attr.ib()
    name: str | None = attr.ib()
    description: str | None = attr.ib()
    tag: str | None = attr.ib()
    avatar_url: str = attr.ib(
        default="https://cdn.discordapp.com/attachments/838613230536359997/884605473167384626/logo128.png"
    )
    timezone: str = attr.ib(default="America/New_York")
    version: int = attr.ib(default=1)

    def to_dict(self) -> dict:
        """Converts this instance to a dict."""
        return attr.asdict(self) | {
            "members": [],
        }


@attr.s
class TupperGroup:
    """Provides a simple model to represent a Tupper fronting group."""

    id_: int = attr.ib()
    name: str | None = attr.ib()
    description: str | None = attr.ib()
    tag: str | None = attr.ib()
    position: int | None = attr.ib()
    user_id: int = attr.ib(default=M_ID)

    def to_dict(self) -> dict:
        """Converts this instance to a dict."""
        return attr.asdict(self)


@attr.s
class PatchworkGroup:  # pylint: disable=R0902
    """Provides a simple model to represent a fronting group."""

    id_: int | None = attr.ib(default=None)
    pluralkit_id: str | None = attr.ib(default=None)
    tupper_id: int | None = attr.ib(default=None)
    name: str = attr.ib(default="Patchwork Collective")
    tag: str = attr.ib(default="| Patchwork Collective")
    avatar_url: str = attr.ib(
        default="https://cdn.discordapp.com/attachments/838613230536359997/884605473167384626/logo128.png"
    )
    description: str = attr.ib(default="[The Patchwork Collective!](http://patchwork.systems/)")
    created_at: datetime.datetime = attr.ib(default=datetime.datetime.now().astimezone(pytz.timezone("US/Eastern")))
    position: int = attr.ib(default=0)
    timezone: str = attr.ib(default="America/New_York")

    def to_dict(self) -> dict:
        """Converts this instance to a dict."""
        return attr.asdict(self)

    def update_from_pluralkit(self, pk_group: PluralKitGroup) -> None:
        """Updates PatchworkGroup from PluralKitGroup."""
        self.name = pk_group.name
        self.pluralkit_id = pk_group.id_
        self.description = pk_group.description
        self.tag = pk_group.tag
        self.avatar_url = pk_group.avatar_url
        self.timezone = pk_group.timezone

    def update_from_tupperbox(self, tp_group: TupperGroup) -> None:
        """Updates PatchworkGroup from TupperGroup."""
        self.name = tp_group.name
        self.tupper_id = tp_group.id_
        self.description = tp_group.description
        self.tag = tp_group.tag
        self.position = tp_group.position

    def to_tupperbox(self) -> TupperGroup:
        return TupperGroup(
            id_=self.tupper_id,
            name=self.name,
            description=self.description,
            tag=self.tag,
            position=self.position,
            user_id=M_ID,
        )

    def to_pluralkit(self) -> PluralKitGroup:
        return PluralKitGroup(
            id_=self.pluralkit_id,
            name=self.name,
            description=self.description,
            tag=self.tag,
            avatar_url=self.avatar_url,
            timezone="America/New_York",
            version=1,
        )

    @classmethod
    def from_tupperbox(cls, tupper: TupperGroup) -> PatchworkGroup:
        return cls(
            tupper_id=tupper.id_,
            name=tupper.name,
            description=tupper.description,
            tag=tupper.tag,
            position=tupper.position,
        )

    @classmethod
    def from_pluralkit(cls, pk: PluralKitGroup) -> PatchworkGroup:
        return cls(
            pluralkit_id=pk.id_,
            name=pk.name,
            description=pk.description,
            tag=pk.tag,
            avatar_url=pk.avatar_url,
            timezone=pk.timezone,
        )

    async def save_to_postgres(self, system_id: int, pg_pool: aiopg.Pool) -> PatchworkGroup:
        logger.info(f"Saving group {self.name} to Postgres Starting!")

        with await pg_pool.cursor() as cursor:
            ins_data = attr.asdict(self, value_serializer=attrs_to_postgres_serializer,) | {
                "system_id": system_id,
                "created_at": datetime.datetime.now().astimezone(pytz.timezone("US/Eastern")),
            }
            ins_data["tupper_id"] = str(self.tupper_id) if self.tupper_id is not None else self.tupper_id
            ins_data["pluralkit_id"] = str(self.pluralkit_id) if self.pluralkit_id is not None else self.pluralkit_id
            try:
                await cursor.execute(
                    """INSERT INTO system_group
                    (pluralkit_id, tupper_id, name, description, tag, avatar_url,
                    timezone, created_at, system_id)
                    VALUES
                    (%(pluralkit_id)s, %(tupper_id)s, %(name)s, %(description)s,
                    %(tag)s, %(avatar_url)s,
                    %(timezone)s, %(created_at)s, %(system_id)s)
                    RETURNING id;""",
                    ins_data,
                )
            except IntegrityError:
                logger.info("Handling IntegrityError now------")
                await cursor.execute(
                    """UPDATE system_group SET
                    pluralkit_id=%(pluralkit_id)s, tupper_id=%(tupper_id)s,
                    name=%(name)s, description=%(description)s, tag=%(tag)s,
                    avatar_url=%(avatar_url)s, timezone=%(timezone)s, system_id=%(system_id)s
                    WHERE id=%(id_)s
                    RETURNING id;""",
                    ins_data,
                )

            id_ = (await cursor.fetchone())[0]
            self.id_ = id_
            logger.info(f"Saved Group {self.name} to System {system_id} ---------------")

            return self

    async def cache(self, member_id: int, redis: aioredis.Redis):
        logger.debug(f"Cached Group {self.name} Starting!")
        redis_key_set = f"{member_id}:group_list"  # noqa: F841
        redis_key_group = f"{member_id}:{self.id_}:group"  # noqa: F841
        group_data = attr.asdict(self, value_serializer=attrs_asdict_to_redis_serializer)  # noqa: F841
        # await redis.hset(redis_key_group, mapping=group_data)

        # await redis.sadd(redis_key_set, group_data["id_"])
        logger.debug(f"Cached Group {self.name} Complete!")

    @classmethod
    async def from_cache(cls, member_id: int, group_id: int, redis: aioredis.Redis) -> PatchworkGroup | None:
        redis_group_key = f"{member_id}:{group_id}:group"
        if await redis.exists(redis_group_key):
            group_data = await redis.hgetall(redis_group_key)
            return cls(**group_data)
        return None

    @staticmethod
    def embed(group: PatchworkGroup, /, ctx: SlashContext, show: bool = False) -> hikari.Embed:
        embed = (
            hikari.Embed(
                title=group.name,
                description=group.description if group.description else "-",
            )
            .add_field(name="Tag:", value=group.tag if group.tag else "-", inline=True)
            .add_field(name="Created At:", value=f"{group.created_at if group.created_at else '-'}", inline=True)
            .add_field(name="Tupper Position:", value=f"{group.position if group.position else '-'}", inline=True)
            .set_thumbnail(group.avatar_url)
            .set_author(name=ctx.member.name, icon=ctx.member.avatar_url)
            .set_footer(
                text=(
                    f"ID: {group.id_} |  "
                    f"PK ID: {group.pluralkit_id if group.pluralkit_id else '-'} |  "
                    f"Tupper ID: {group.tupper_id if group.tupper_id else '-'}"
                )
            )
        )
        if not show:
            embed.add_field(name="Timezone:", value=group.timezone if group.timezone else "-", inline=True)

        return embed
