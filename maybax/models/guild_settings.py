"""Provides a Model for interacting with GuildSettings"""
from __future__ import annotations

import logging

import aiopg
import aioredis
import attr
import hikari
import tanjun
from psycopg2.extras import RealDictCursor
from util.postgres_helpers import ensure_guild_settings_exist, set_guild_proxy_role

logger = logging.getLogger(__name__)


LOCAL_GUILD_SETTINGS = "{guild_id}:local_guild_settings"
ALLOWED_PROXY_KEY = "{guild_id}:allowed_proxy_role_id"


@attr.s
class LocalGuildSettings:
    guild_id: str = attr.ib()
    logging_channel: int | None = attr.ib(default=None)
    vc_logging_channel: int | None = attr.ib(default=None)
    mod_logging_channel: int | None = attr.ib(default=None)
    system_role: int | None = attr.ib(default=None)

    @classmethod
    async def bot_startup_setup(
        cls, ctx: tanjun.SlashContext, redis: aioredis.Redis, pg_pool: aiopg.Pool
    ) -> LocalGuildSettings | None:
        if not ctx.guild_id:
            return None
        guild_db_id = await ensure_guild_settings_exist(ctx.guild_id, pg_pool)
        with await pg_pool.cursor(cursor_factory=RealDictCursor) as cursor:
            await cursor.execute(
                """SELECT
                gos.guild_id, gos.logging_channel, gos.vc_logging_channel, gos.mod_logging_channel, sgs.system_role
                FROM guildownersettings_guildownersettings as gos
                LEFT JOIN system_guildsystemsettings as sgs ON gos.id=sgs.guild_id
                WHERE gos.id=%(id)s;""",
                {"id": guild_db_id},
            )
            result = await cursor.fetchone()
            if not result["guild_id"]:
                return None
            logger.info(f"Found LocalGuildSettings {ctx.guild_id} in Postgres")
            guild_key = LOCAL_GUILD_SETTINGS.format(guild_id=ctx.guild_id)
            if result["logging_channel"] is None:
                del result["logging_channel"]
            if result["vc_logging_channel"] is None:
                del result["vc_logging_channel"]
            if result["mod_logging_channel"] is None:
                del result["mod_logging_channel"]
            if result["system_role"] is None:
                del result["system_role"]
            await redis.hmset(
                guild_key,
                mapping=result,
            )
            return cls(**result)

    @classmethod
    async def setup(
        cls, ctx: tanjun.SlashContext, redis: aioredis.Redis, pg_pool: aiopg.Pool
    ) -> LocalGuildSettings | None:
        if not ctx.guild_id:
            return None
        guild_key = LOCAL_GUILD_SETTINGS.format(guild_id=ctx.guild_id)
        if (await redis.exists(guild_key)) == 1:
            local_settings = await redis.hgetall(guild_key)
            logger.info(f"Found LocalGuildSettings {guild_key} in Redis")
            proxy_key = ALLOWED_PROXY_KEY.format(guild_id=ctx.guild_id)
            if (await redis.exists(proxy_key)) == 1:
                local_settings["system_role"] = await redis.get(proxy_key)
                logger.info(f"Found LocalGuildSettings {local_settings['system_role']} in Redis")

            return cls(**local_settings)
        else:
            settings_from_db = await cls.bot_startup_setup(ctx, redis, pg_pool)
            return settings_from_db

    async def set_proxy_role_for_guild(
        self, ctx: tanjun.SlashContext, allowed_role: hikari.Role, redis: aioredis.Redis, pg_pool: aiopg.Pool
    ) -> bool:
        guild = ctx.get_guild()
        if not guild:
            return False
        guild_key = ALLOWED_PROXY_KEY.format(guild_id=guild.id)
        if await redis.exists(guild_key) == 0 and allowed_role.id == (await redis.get(guild_key)):
            await ctx.respond(f"Your Proxy role is already set to {allowed_role.mention}!")
            return False
        await redis.set(guild_key, allowed_role.id)
        guild_db_id = await ensure_guild_settings_exist(guild.id, pg_pool)
        if not guild_db_id:
            return False
        saved_to_db = await set_guild_proxy_role(guild_db_id, allowed_role.id, pg_pool)
        logger.info(f"Saved Proxy Role {allowed_role.id} for Guild {guild.id}")
        await redis.hset(guild_key, key="system_role", value=allowed_role.id)
        return saved_to_db

    async def set_logging_channel(
        self,
        ctx: tanjun.SlashContext,
        logging_channel: hikari.GuildTextChannel,
        redis: aioredis.Redis,
        pg_pool: aiopg.Pool,
    ) -> bool:
        guild = ctx.get_guild()
        if not guild:
            return False
        try:
            with await pg_pool.cursor() as cursor:
                await cursor.execute(
                    """UPDATE guildownersettings_guildownersettings
                    SET logging_channel=%(logging_channel)s
                    WHERE guild_id=%(guild_id)s;""",
                    {"logging_channel": logging_channel.id, "guild_id": guild.id},
                )

            guild_key = LOCAL_GUILD_SETTINGS.format(guild_id=guild.id)
            await redis.hset(guild_key, key="logging_channel", value=logging_channel.id)
            return True
        except Exception:
            logger.exception("Postgres Exception")
            return False

    async def set_vc_logging_channel(
        self,
        ctx: tanjun.SlashContext,
        vc_logging_channel: hikari.GuildTextChannel,
        redis: aioredis.Redis,
        pg_pool: aiopg.Pool,
    ) -> bool:
        guild = ctx.get_guild()
        if not guild:
            return False
        try:
            with await pg_pool.cursor() as cursor:
                await cursor.execute(
                    """UPDATE guildownersettings_guildownersettings
                    SET vc_logging_channel=%(vc_logging_channel)s
                    WHERE guild_id=%(guild_id)s;""",
                    {"vc_logging_channel": vc_logging_channel.id, "guild_id": guild.id},
                )

            guild_key = LOCAL_GUILD_SETTINGS.format(guild_id=guild.id)
            await redis.hset(guild_key, key="vc_logging_channel", value=vc_logging_channel.id)
            return True
        except Exception:
            logger.exception("Postgres Exception")
            return False

    async def set_mod_logging_channel(
        self,
        ctx: tanjun.SlashContext,
        mod_logging_channel: hikari.GuildTextChannel,
        redis: aioredis.Redis,
        pg_pool: aiopg.Pool,
    ) -> bool:
        guild = ctx.get_guild()
        if not guild:
            return False
        try:
            with await pg_pool.cursor() as cursor:
                await cursor.execute(
                    """UPDATE guildownersettings_guildownersettings
                    SET mod_logging_channel=%(mod_logging_channel)s
                    WHERE guild_id=%(guild_id)s;""",
                    {"mod_logging_channel": mod_logging_channel.id, "guild_id": guild.id},
                )

            guild_key = LOCAL_GUILD_SETTINGS.format(guild_id=guild.id)
            await redis.hset(guild_key, key="mod_logging_channel", value=mod_logging_channel.id)
            return True
        except Exception:
            logger.exception("Postgres Exception")
            return False

    async def guild_settings_embed(self, ctx: tanjun.SlashContext):
        guild = ctx.get_guild()
        if not guild:
            return False
        default = logging_channel = vc_logging_channel = mod_logging_channel = system_role = "`[ Not Set ]`"
        if logging_channel := (await self.get_logging_channel(ctx)):
            logging_channel = logging_channel.mention
        if vc_logging_channel := (await self.get_vc_logging_channel(ctx)):
            vc_logging_channel = vc_logging_channel.mention
        if mod_logging_channel := (await self.get_mod_logging_channel(ctx)):
            mod_logging_channel = mod_logging_channel.mention
        if system_role := await self.get_system_role(ctx):
            system_role = system_role.mention
        embed = (
            hikari.Embed(title=f"Server Settings for {guild.name}")
            .add_field(name="Logging Channel:", value=logging_channel or default, inline=True)
            .add_field(name="VC Logging Channel:", value=vc_logging_channel or default, inline=True)
            .add_field("Mod Logging Channel:", value=mod_logging_channel or default, inline=True)
            .add_field(name="System Role:", value=system_role or default)
            .set_thumbnail(guild.icon_url)
            .set_footer("You can also change settings on the Maybax Website!")
        )
        return embed

    async def get_logging_channel(self, ctx: tanjun.SlashContext) -> hikari.GuildTextChannel | None:
        if not self.logging_channel:
            return None
        return (
            ctx.cache.get_guild_channel(self.logging_channel)
            if ctx.cache and ctx.cache.get_guild_channel(self.logging_channel)
            else (await ctx.rest.fetch_channel(self.logging_channel))
        )

    async def get_vc_logging_channel(self, ctx: tanjun.SlashContext) -> hikari.GuildTextChannel | None:
        if not self.vc_logging_channel:
            return None
        return (
            ctx.cache.get_guild_channel(self.vc_logging_channel)
            if ctx.cache and ctx.cache.get_guild_channel(self.vc_logging_channel)
            else (await ctx.rest.fetch_channel(self.vc_logging_channel))
        )

    async def get_mod_logging_channel(self, ctx: tanjun.SlashContext) -> hikari.GuildTextChannel | None:
        if not self.mod_logging_channel:
            return None
        return (
            ctx.cache.get_guild_channel(self.mod_logging_channel)
            if ctx.cache and ctx.cache.get_guild_channel(self.mod_logging_channel)
            else (await ctx.rest.fetch_channel(self.mod_logging_channel))
        )

    async def get_system_role(self, ctx: tanjun.SlashContext) -> hikari.Role | None:
        if not self.system_role:
            return None
        system_role = None
        if not self.system_role:
            return None
        if ctx.cache:
            guild_roles = ctx.cache.get_roles_view_for_guild(ctx.guild_id)
            system_role = guild_roles.get(self.system_role) if guild_roles.get(self.system_role) else None
        if not system_role:
            guild_roles = await ctx.rest.fetch_roles(guild=ctx.guild_id)
            for guild_role in guild_roles:
                if guild_role.id == int(self.system_role):
                    system_role = guild_role
                    break

        return system_role
