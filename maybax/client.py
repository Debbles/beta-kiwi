"""Module provides the basic functions to build the client and bot."""
from __future__ import annotations

import logging
import typing

import aioredis
import config as config_
import hikari
import httpx
import tanjun
import yuyo
from hikari import config as hikari_config
from models.front.system import SystemModel
from util.dependencies import (
    HttpxInjector,
    PatchworkDep,
    PostgresPoolDependency,
    RedisPoolDependency,
)
from util.hooks import on_error, on_parse_error

logger = logging.getLogger()


def build_bot() -> hikari.GatewayBot:
    """Builds and returns a GatewayBot. Calls build_client to start tanjun."""
    config = config_.FullConfig.from_environ()
    logger.setLevel(config.log_level)
    bot = hikari.GatewayBot(
        config.tokens.discord_bot,
        logs=config.log_level,
        intents=config.intents,
        cache_settings=hikari_config.CacheSettings(components=config.cache),
    )
    client = build_client(bot, config=config)

    setup_hooks(client, config)

    set_client_dependencies(client, bot, config)

    load_plugins(client)

    return bot


def build_client(bot: hikari.GatewayBot, /, *, config: config_.FullConfig | None = None) -> tanjun.Client:
    """Builds and configures the tanjun client from a given GatewayBot."""
    if not config:
        config = config_.FullConfig.from_environ()

    client = tanjun.Client.from_gateway_bot(
        bot,
        mention_prefix=config.mention_prefix,
        declare_global_commands=config.set_global_commands,
    )

    return client


def setup_hooks(client: tanjun.Client, config: config_.FullConfig) -> tanjun.Client:
    if config.owner_only:
        client.set_check(tanjun.checks.OwnerCheck())

    client.set_hooks(tanjun.AnyHooks().set_on_parser_error(on_parse_error).set_on_error(on_error))

    return client


def set_client_dependencies(client: tanjun.Client, bot: hikari.GatewayBot, config) -> tanjun.Client:
    """Setup DI Dependencies and Callbacks on the tanjun Client"""
    # Create component and reaction client for yuyo.
    component_client = yuyo.ComponentClient.from_gateway_bot(bot, event_managed=False)
    reaction_client = yuyo.ReactionClient.from_gateway_bot(bot, event_managed=False)

    # Add prefxes to bot, if set
    client.add_prefix(config.prefixes)

    # Add callbacks for client.
    (
        # Yuyo Callbacks for components events
        client.add_client_callback(tanjun.ClientCallbackNames.STARTING, component_client.open)
        .add_client_callback(tanjun.ClientCallbackNames.CLOSING, component_client.close)
        # Yuyo Callbacks for reaction events
        .add_client_callback(tanjun.ClientCallbackNames.STARTING, reaction_client.open)
        .add_client_callback(tanjun.ClientCallbackNames.CLOSING, reaction_client.close)
    )

    client.add_prefix(config.prefixes)
    PostgresPoolDependency().load_into_client(client)

    (
        client.set_type_dependency(yuyo.ComponentClient, component_client)
        .set_type_dependency(yuyo.ReactionClient, reaction_client)
        .set_type_dependency(config_.FullConfig, lambda: typing.cast(config_.FullConfig, config))
        .set_type_dependency(config_.Tokens, lambda: typing.cast(config_.FullConfig, config).tokens)
        .set_type_dependency(httpx.AsyncClient, HttpxInjector().spawn_client())
        .set_type_dependency(SystemModel, PatchworkDep.setup())
        .set_type_dependency(aioredis.Redis, RedisPoolDependency().setup_connection())
    )

    return client


def load_plugins(client: tanjun.Client) -> tanjun.Client:
    (
        client.load_modules("plugins.front")
        .load_modules("plugins.systems")
        .load_modules("plugins.members")
        .load_modules("plugins.groups")
        .load_modules("plugins.proxies")
        .load_modules("plugins.faq")
        .load_modules("plugins.grounding_exercises")
        .load_modules("plugins.debug")
        .load_modules("plugins.st_jude")
        .load_modules("plugins.guild_settings")
    )
    return client
