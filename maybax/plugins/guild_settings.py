"""Plugin for providing an interface to change a guild's settings."""
from __future__ import annotations

import logging

import aiopg
import aioredis
import hikari
import tanjun
from checks.guild_owner_check import guild_owner_check
from models.guild_settings import LocalGuildSettings
from tanjun import SlashContext
from tanjun.checks import with_check

logger = logging.getLogger(__name__)

component = tanjun.Component()

guild_settings = component.with_slash_command(  # pylint: disable=C0103
    tanjun.slash_command_group("settings", "Change your Servers settings!")
)


@guild_settings.with_command
@with_check(guild_owner_check)
@tanjun.as_slash_command("list", "View Maybax's settings for this Server.")
async def _list(
    ctx: SlashContext,
    redis: aioredis.Redis = tanjun.injected(type=aioredis.Redis),
    pg_pool: aiopg.Pool = tanjun.injected(type=aiopg.Pool),
):
    local_guild_settings = await LocalGuildSettings.setup(ctx, redis, pg_pool)
    if not local_guild_settings:
        await ctx.respond("This guild is not registered.")
        return
    embed = await local_guild_settings.guild_settings_embed(ctx)
    await ctx.respond(embed)


@guild_settings.with_command
@with_check(guild_owner_check)
@tanjun.with_channel_slash_option("logging_channel", "Change the channel Maybax logs general information to.")
@tanjun.as_slash_command("set_logging_channel", "Change the channel Maybax logs to.")
async def set_logging_channel(
    ctx: SlashContext,
    logging_channel: hikari.GuildTextChannel,
    redis: aioredis.Redis = tanjun.injected(type=aioredis.Redis),
    pg_pool: aiopg.Pool = tanjun.injected(type=aiopg.Pool),
):
    local_guild_settings = await LocalGuildSettings.setup(ctx, redis, pg_pool)
    if not local_guild_settings:
        await ctx.respond("This guild is not registered.")
        return
    confirmed = await local_guild_settings.set_logging_channel(ctx, logging_channel, redis, pg_pool)
    if confirmed:
        await ctx.respond(f"Updated your logging channel to {logging_channel.name}")
        return
    await ctx.respond(":(")


@guild_settings.with_command
@with_check(guild_owner_check)
@tanjun.with_channel_slash_option("vc_logging_channel", "Change the channel Maybax logs general information to.")
@tanjun.as_slash_command("set_vc_logging_channel", "Change the channel Maybax logs to.")
async def set_vc_logging_channel(
    ctx: SlashContext,
    vc_logging_channel: hikari.GuildTextChannel,
    redis: aioredis.Redis = tanjun.injected(type=aioredis.Redis),
    pg_pool: aiopg.Pool = tanjun.injected(type=aiopg.Pool),
):
    local_guild_settings = await LocalGuildSettings.setup(ctx, redis, pg_pool)
    if not local_guild_settings:
        await ctx.respond("This guild is not registered.")
        return
    confirmed = await local_guild_settings.set_vc_logging_channel(ctx, vc_logging_channel, redis, pg_pool)
    if confirmed:
        await ctx.respond(f"Updated your logging channel to {vc_logging_channel.name}")
        return
    await ctx.respond(":(")


@guild_settings.with_command
@with_check(guild_owner_check)
@tanjun.with_channel_slash_option("mod_logging_channel", "Change the channel Maybax logs general information to.")
@tanjun.as_slash_command("set_mod_logging_channel", "Change the channel Maybax logs to.")
async def set_mod_logging_channel(
    ctx: SlashContext,
    mod_logging_channel: hikari.GuildTextChannel,
    redis: aioredis.Redis = tanjun.injected(type=aioredis.Redis),
    pg_pool: aiopg.Pool = tanjun.injected(type=aiopg.Pool),
):
    local_guild_settings = await LocalGuildSettings.setup(ctx, redis, pg_pool)
    if not local_guild_settings:
        await ctx.respond("This guild is not registered.")
        return
    confirmed = await local_guild_settings.set_mod_logging_channel(ctx, mod_logging_channel, redis, pg_pool)
    if confirmed:
        await ctx.respond(f"Updated your logging channel to {mod_logging_channel.name}")
        return
    await ctx.respond(":(")


@guild_settings.with_command
@with_check(guild_owner_check)
@tanjun.with_role_slash_option(
    "allowed_role", "Setting this role allows anyone with it to use use Maybax's Proxy Functions."
)
@tanjun.as_slash_command(
    "set_proxy_role", "Allows a Guild owner to set a role allowing Members to use the Proxy Functions."
)
async def set_proxy_role(
    ctx: SlashContext,
    allowed_role: hikari.Role,
    redis: aioredis.Redis = tanjun.injected(type=aioredis.Redis),
    pg_pool: aiopg.Pool = tanjun.injected(type=aiopg.Pool),
):
    local_guild_settings = await LocalGuildSettings.setup(ctx, redis, pg_pool)
    if not local_guild_settings:
        await ctx.respond("This guild is not registered.")
        return
    confirmed = await local_guild_settings.set_proxy_role_for_guild(ctx, allowed_role, redis, pg_pool)
    if confirmed:
        await ctx.respond(f"Proxying is now limited to {allowed_role.mention}!")
        return
    await ctx.respond(":(")


@tanjun.as_loader
def load_examples(client: tanjun.abc.Client) -> None:
    """Default tanjun loader."""
    client.add_component(component.copy())


@tanjun.as_unloader
def unload(client: tanjun.Client, /) -> None:
    client.remove_component_by_name(component.name)
