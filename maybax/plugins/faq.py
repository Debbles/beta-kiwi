"""Provides embeds giving information about frequently asked questions
on DID/OSDD"""
from __future__ import annotations

from random import choice

import hikari
import tanjun
from tanjun import SlashContext

TALKING_BOTS = (
    "These `[BOT]` messages are commonly called 'proxies' or 'Proxied Messages' "
    "and are created using bots like TupperBox, Pluralkit or Myself!\n"
    "All of these messages are written by a real person and it is not funny or "
    "original to joke about it. We have heard it more times than you can imagine, "
    "I promise.\n"
)

TONE_INDICATOR_DICT = {
    "/srs": "Serious",
    "/nsrs": "Not Serious",
    "/p": "Platonic",
    "/t": "Teasing",
    "/nm": "Not Mad",
    "/lu": "Little Upset",
    "/neg or /nc": "Negative Connotation",
    "/pos or /pc": "Positive Connotation",
    "/lh": "Light Hearted",
    "/nbh": "Nobody Here",
    "/m": "Metaphorically",
    "/li": "Literally",
    "/ij": "Inside Joke",
    "/j": "Joke/Joking",
    "/hj": "Half Joking",
    "/rh or /rt": "Rhetorical Question",
    "/gen or /g": "Genuine (Not Sarcastic)",
    "/s": "Sarcastic",
    "/hyp": "Hyperbole",
}

TONE_IND_OPT = list(TONE_INDICATOR_DICT.keys())
TONE_IND_OPT.append("All")


component = tanjun.Component()

faq = component.with_slash_command(tanjun.slash_command_group("faq", "Learn about something!"))  # pylint: disable=C0103


@faq.with_command
@tanjun.with_str_slash_option(
    "indicator",
    "The indicator you want to look up (ex: /s) *Optional*.",
    choices=TONE_IND_OPT,
    default="All",
)
@tanjun.as_slash_command("tone", "Learn about tone indicators")
async def tone_indicators(ctx: SlashContext, indicator: str = "all"):
    """Provides an embed that explains tone indicators.

    Accepts a tone indcator to search. For example `/s`.

    Parameters
    ==========
    indicator: str
        The tone indicator to search by.
    """
    description = (
        "Interpreting messages online can be hard! A lot of people "
        "struggle with issues like 'are they being sarcastic?' or 'that "
        'feels like they are mocking me...\' because the "tone" or intent '
        "of a message was mis-read. \n\nTone Indicators help with that! "
        "\n\nJust add an indicator to end of a message to help better express "
        "what you mean! /pos\n"
    )
    embed = (
        hikari.Embed(
            title="What are Tone Indicators?",
            description=description,
        )
        .set_author(name=ctx.member.username, icon=ctx.member.avatar_url)
        .set_footer(text="Submit more at https://gitlab.com/aster.codes/maybax2")
    )

    if indicator.lower() != "all":
        if found_indicator := TONE_INDICATOR_DICT.get(indicator.lower()):
            embed.add_field(
                name=indicator,
                value=found_indicator,
                inline=False,
            )
    else:
        for indicator, definition in TONE_INDICATOR_DICT.items():
            embed.add_field(
                name=indicator,
                value=definition,
                inline=True,
            )

    if guild := ctx.get_guild():
        embed.set_thumbnail(guild.icon_url)

    await ctx.respond(embed=embed)


plurality = faq.with_command(tanjun.slash_command_group("plurality", "Learn about Plurality!"))  # pylint: disable=C0103


@plurality.with_command
@tanjun.as_slash_command("what-is", "Learn what Plurality is!")
async def what_is_plurality(ctx: SlashContext) -> None:
    """Provides a command written by the Patchwork Collective that
    explains how DID/OSDD is."""
    colors = [1842204, 16428121, 16777215]
    color = choice(colors)  # noqa: S311
    guild = ctx.get_guild()
    description = (
        f"Welcome to {guild.name}! \nIt looks like "
        "you're curious about DID and OSDD! We aim to be as "
        "inclusive as possible, so we have a number of great "
        "resources here to help. Below are a links that might "
        "help!"
    )
    embed = hikari.Embed(title="What is DID/OSDD", description=description, colour=color)
    embed.set_thumbnail(
        "https://static.wixstatic.com/media/be2cc9_c5bef83e322"
        "d44a2ad9bf196a1951e3b~mv2.jpg/v1/fill/w_224,h_150,al_"
        "c,q_80,usm_0.66_1.00_0.01/IMG_2918_JPG.webp"
    )

    embed.add_field(
        name="Patchwork Collectives Intro to Plurality Series",
        value="The Patchwork Collective wrote a short series "
        "going over a what exactly [Plurality is](https://pat"
        "chwork.systems/plurality/what-is-plurality.html)",
        inline=False,
    )
    embed.add_field(
        name="SciShow Video on Dissociative Identity Disorder:",
        value=(
            "Hank Green presents a [5 minute video]"
            "(https://www.youtube.com/watch?v=l4hVtBV5o4s) going over "
            "the basics of Dissociative Identity Disorders and how the "
            "media misportrays these disorders."
        ),
        inline=False,
    )
    embed.add_field(
        name="Plurality Hub Carrd",
        value=(
            "The Heretic created a great [carrd site]"
            "(https://plurality-hub.carrd.co/#directory) with tons of "
            "introductory material, such as terminology and etiquette."
        ),
        inline=False,
    )
    embed.add_field(
        name="D-I-D You Know Graphic",
        value=(
            "tigrin on DeviantArt provides a [great explanation of"
            " Dissociative Disorders in an infographic comic]"
            "(https://www.deviantart.com/tigrin/art/D-I-D-You-Know-58072489)."
            " They go over what DID is like and how Hollywood portrays "
            "it incorrectly."
        ),
        inline=False,
    )
    embed.add_field(
        name="BetterTogether's DID/OSDD Casually and Visually Explained",
        value=(
            "BetterTogether's tumblr offers many great DID/OSDD "
            "resources, including their wonderful "
            "[DID/OSDD Casually Explained]"
            "(https://clever-and-unique-name.tumblr.com/post/187704319719/didosdd-casually-explained-masterpost)."
            " Great explanations in a very simple to understand visual "
            "style!"
        ),
        inline=False,
    )
    await ctx.respond(embed=embed)


@plurality.with_command
@tanjun.as_slash_command("talking-bots", "Why does it look like the [BOT] accounts are talking?")
async def talking_bots(ctx: SlashContext) -> None:
    colors = [1842204, 16428121, 16777215]
    color = choice(colors)  # noqa: S311
    embed = (
        hikari.Embed(title="Why are accounts with a 'BOT' tag talking?", description=TALKING_BOTS, colour=color)
        .add_field(
            name="Why?",
            value="Members with DID/OSDD often use Proxy Messages to help indicate who is fronting.",
            inline=False,
        )
        .add_field(
            name="What is DID/OSDD?",
            value=(
                "We're glad you asked! We actually have a lot of information about DID/OSDD in this bot!"
                " Checkout all of the commands under ```/faq plurality```"
                "```/faq plurality what-is``` Gives an intro to DID/OSDD"
                "If you have any questions, we are generally happy to help educate. Just please make sure to "
                "ask respectfully and read some of the information first (especially `/faq plurality manners`!)."
            ),
            inline=False,
        )
        .add_field(
            name="Be Respectful",
            value="Please remember to be respectful to all members regardless of any disabilities.",
            inline=False,
        )
    )
    await ctx.respond(embed=embed)


@plurality.with_command
@tanjun.as_slash_command("parts-affecting", "Learn how Parts/Alters affect each other!")
async def parts_affecting(ctx: SlashContext):
    """Provides a command written by the Patchwork Collective that
    explains how DID/OSDD Parts/Alters affect each other."""
    colors = [16412539, 5798125, 16763174]
    color = choice(colors)  # noqa: S311
    embed = hikari.Embed(
        title="How Alters/Parts Affect Each Other",
        description=(
            "The experiences described below are informally or loosely defined terms "
            "for common DID/OSDD system experiences. These terms/experiences may not "
            "be applicable to every system. Every system may not experience these "
            "terms in the same way. Some systems may even have additional experiences"
            " not listed."
        ),
        colour=color,
    )
    embed.set_thumbnail(
        "https://cdn.discordapp.com/attachments/8386132"
        "30536359997/863454018545516605/70351780a934458"
        "9c41b28b2bba621f1aaa8b2e0.png"
    )
    embed.add_field(
        name="BetterTogether's How Alters/Parts Are Experienced Internally",
        value=(
            "This tumblr artist [provides a wonderful visual explanation]"
            "(https://clever-and-unique-name.tumblr.com/po"
            "st/185276103179/thank-you-dissociatingdingo-for-being-my-editor)"
            " of how Alters commonly experience other "
            "Alters internally. "
            "The infographic covers idea's like "
            '"passive influence," "dormancy" and more!'
        ),
        inline=False,
    )
    embed.add_field(
        name="BetterTogether's Parts of Self",
        value=(
            "Alters perception of how their consciousness "
            "exists with other alters consciousness (in "
            "their system) can fluctuate. This perception"
            " can change multiple times an hour or stay "
            "consistent for long periods. [BetterTogether"
            " provides a great infographic]"
            "(https://clever-and-unique-name.tumblr.com/post/185228806634/since-im-still-waiting-for-replacement-pen-tips)"
            " that visually explains a few of these states, "
            "including CoCon'ing and Blurring/Blurry."
        ),
        inline=False,
    )
    embed.add_field(
        name="BetterTogether's One Person's Experience Having Dissociative Parts",
        value=(
            "[BetterTogether provides their personal "
            "experience having Dissociative Parts.]"
            "(https://clever-and-unique-name.tumblr.com/post/641471968521109504/i-made-a-thing-thats-entirely-based-off-personal)"
            " It is a wonderful piece that covers many"
            " common questions."
        ),
        inline=False,
    )
    await ctx.respond(embed=embed)


@plurality.with_command
@tanjun.as_slash_command("split", "Learn about Splitting")
async def split(ctx: SlashContext) -> None:
    """Provides a command written by the Patchwork Collective that
    explains DID/OSDD switching."""
    embed = hikari.Embed(title="Splitting")
    embed.set_thumbnail(
        "https://64.media.tumblr.com/f6f2c5de5188d8b"
        "a4c1187cdafc4ce09/0b272e3ec895fac3-d4/s400x"
        "600/de76a995c7e8e0a6cf3b054ec8f452a5747db84f.png"
    )
    embed.add_field(
        name="The Entropy System - Splitting - When a New Identity Forms",
        value=(
            "EntropySystem makes loads of wonderful "
            "DID/OSDD awareness videos on Youtube! "
            "In [this video they explain what splitting "
            "means](https://www.youtube.com/watch?v=AgB29CCcGO4)"
            ", how it affects Systems, and recounts"
            " a personal experience."
        ),
        inline=False,
    )
    embed.add_field(
        name="BetterTogether's Splits in Established Systems",
        value=(
            "[This infographic explains what splitting is]"
            "(https://clever-and-unique-name.tumblr.com/post/189763970544/there-is-no-logical-or-natural-limit-to-how)"
            " and why it can occur in Systems that seem "
            'to be "doing fine".'
        ),
        inline=False,
    )
    await ctx.respond(embed=embed)


@plurality.with_command
@tanjun.as_slash_command("switch", "Learn about Switching")
async def switching(ctx: SlashContext) -> None:
    """Explains what switching is!"""
    colors = [16758273, 2863491]
    color = choice(colors)  # noqa: S311
    embed = hikari.Embed(
        title="Switching",
        description=(
            "Switching is when one Part/Alter exchanges control "
            "of parts or all of the body with another Alter. "
            'This could be as small as letting one Part "Switching"'
            "into a body part as small as a finger, or even the whole"
            "/entire body!\n\n"
            "Switching is not always on purpose! Some Parts can "
            "force switches or prevent them, like GateKeepers."
        ),
        color=color,
    )
    embed.add_field(
        name='"Switchy"',
        value=(
            "A feeling that many Systems experience. It "
            "describes feeling like an Alter is close to "
            "switching but is not. It can also mean feeling "
            "an Alter is trying to force a switch."
        ),
        inline=False,
    )
    embed.set_thumbnail(
        "https://64.media.tumblr.com/17faccb62cd439db26"
        "300cdf333cbf49/e64c806c1fdb7a70-00/s400x600/56d"
        "02eaa0db306afef458305df12c7aee5273979.png"
    )
    await ctx.respond(embed=embed)


@plurality.with_command
@tanjun.as_slash_command("system_pride", "Learn about System Pride.")
async def system_pride(ctx: SlashContext) -> None:
    """Provides an embed that explains System Pride Day!"""
    colors = [1842204, 16428121, 16777215]
    color = choice(colors)  # noqa: S311
    embed = hikari.Embed(
        title="Learn about System Pride!",
        description=(
            "The Entropy System [coined the idea of a System Pride Day and "
            "System Pride Flag and explains in this video](https://www.you"
            "tube.com/watch?v=1p2khQLlGIM)"
            ". Below are a few pieces of text [from their official System "
            "Pride Website](https://theentropysystemdi.wixsite.com/systemp"
            "rideday).\n\n"
            "Dissociative Identity Disorder and disorders like it have been"
            " misunderstood and misdiagnosed since the beginning. People "
            "have misdiagnosed these disorders as everything from Borderline"
            " Personality Disorder to demonic possession. None of that is true."
            "\n\n"
            "Dissociative Identity Disorder (DID) and Otherwise Specified "
            "Dissociative Disorder (OSDD) are developed as the result of a child"
            " experiencing severe, prolonged trauma. This trauma could be abuse,"
            " neglect, multiple invasive surgeries in childhood, multiple natural"
            " disasters, or anything else that would make a child afraid and feel"
            " unable to escape. As a survival response, the brain prevents the"
            " child's mind from forming one cohesive identity as it does in most "
            'people. It instead keeps the trauma divided in separate "containers"'
            " that each develop their own sense of identity. These are commonly "
            "referred to as alters, though some may call them parts, headmates, "
            "personalities, or something they personally feel describes them "
            "best. A person who has developed multiple parts in this way is "
            "referred to as a Multiple or a System and lives life experiencing "
            "multiplicity.\n\n"
            "Thanks to endless negative representations in the media, Systems"
            " have been conditioned to live in fear and shame should someone "
            "discover their multiplicity. Many are too frightened to seek help"
            " from friends, family, and even professionals. We here in the Entropy"
            " System decided this needed to change. Being Multiple isn't shameful. "
            "It is a sign of survival. All the alters in a system were born to help "
            "a mind survive terrible things and had to work to work together to get "
            "through it. From our perspective, that is something to celebrate! We"
            " love each member of our system and we are proud of how far we've "
            "come together. We want other systems to feel this way, too.\n\n"
            "That is why we proposed System Pride Day and the System Pride flag."
        ),
        color=color,
    )
    embed.set_thumbnail(
        "https://static.wixstatic.com/media/be2cc9_c5bef83e322d44a2ad9bf196"
        "a1951e3b~mv2.jpg/v1/fill/w_224,h_150,al_c,q_80,usm_0.66_1.00_0.01/IMG_2918_JPG.webp"
    )
    embed.add_field(
        name="System Pride Flag Colors Explained",
        value=(
            "**Black Strike** - represents people who don't yet "
            "know that they are a part of a system or who still "
            "have a lot of trouble communicating with their system.\n"
            "**The Orange Stripe** - represents systems who are "
            "living with functional multiplicity. This means that "
            "they either have not yet achieved integration or are "
            "choosing not to integrate but are still able to work "
            "together as a team to live a happy, functional life.\n"
            "**The White Stripe** - represents systems that have"
            " achieved full fusion. They may not be multiple "
            "anymore but they were at one point and we still "
            "recognize them as a part of the community."
        ),
    )
    await ctx.respond(embed=embed)


@plurality.with_command
@tanjun.as_slash_command("manners", "Learn about Manners with Systems")
async def manners(ctx: SlashContext) -> None:
    """
    Some behavior can be very rude and uncomfortable for Systems. If you're not familiar, read this!
    """
    colors = [16758273, 2863491]
    color = choice(colors)  # noqa: S311
    embed = hikari.Embed(
        title="Courtesy with Systems",
        description=(
            "It can be hard to know what is and is not "
            "appropriate to do with systems. This will "
            "explain the general rule of thumbs, but "
            "this is not the end all be all. Some "
            "systems are okay with things that others "
            "are not."
        ),
        color=color,
    )
    embed.set_thumbnail(
        "https://64.media.tumblr.com/91549526d7d6eb4671b5149fbd2ba607/"
        "7ec43f85249903a1-51/s500x750/ec0304f0a30195e474610f5c6d09bd0a8873255a.png"
    )
    embed.add_field(
        name="Alters are not the same person, please do not treat them as such.",
        value=(
            "The most important thing to remember is that each Alter is a "
            "*different person*. Treating an alter like "
            "another alter, is generally frowned upon and "
            "can be seen as very disrespectful, if not "
            "downright ableist."
        ),
    )
    embed.add_field(
        name="Asking for other Alters",
        value=(
            "It is also considered a bit disrespectful to "
            "call upon another alter. being that, if you "
            'have an alter you\'re friends with, "A", and '
            '"B" is fronted, it\'s considered rude to ask '
            '"B" when "A" will be out, or to pass on '
            "messages for them. This varies from system to "
            "system, but a rule of thumb is to ask beforehand."
        ),
        inline=False,
    )
    embed.add_field(
        name='Don\'t use DID/OSDD or Parts/Alters as a "cool thing," or "trend" to talk about.',
        value=(
            "Another thing that is often seen as "
            "disrespectful, is talking over us about our "
            'experiences, or inputting your "personal '
            'feelings" on things relating to being plural. '
            "We are the ones with authority over plurality, "
            "and if you are a singlet your opinions on us "
            "are, 99% of the time, not appreciated."
        ),
        inline=False,
    )
    embed.add_field(
        name="Do not talk about plurality in a non-personal way",
        value=(
            "Plural peope are not a science project or a specimen for you to examine."
            " Talking about how plurality is cool or interesting, or "
            "talk in depth about how you have done excessive research"
            " often comes off as creepy."
        ),
        inline=False,
    )
    embed.add_field(
        name="Acting like Alters/Parts have a choice in who they are",
        value=(
            "You do not have a choice in your personality, "
            "alters/parts do not either. Every Alter/Part "
            "is responsible for their actions, but personality"
            "traits, sources, gender, and presentation is not "
            "something anyone can control, Alters/Parts are no "
            "exception."
        ),
        inline=False,
    )
    embed.add_field(
        name="Do not positive trigger an alter out without permission",
        value=(
            'Many alters/parts will have "positive" or '
            '"posi" triggers. These triggers, which are '
            "actions, phrases, noises, etc, can cause or "
            "force an alter/part to the front. It is considered"
            " rude to do this without prior permission from "
            "both the alter fronting and the alter being moved"
            " to the front."
        ),
        inline=False,
    )
    embed.add_field(
        name="Don't make jokes about proxied messages being bots",
        value="We've heard it a million times. It's not funny. It was never funny.",
        inline=False,
    )
    embed.set_footer(
        text="Sourced from The Heretic System's PluralityHub Carrd https://plurality-hub.carrd.co/#courtesy."
    )
    await ctx.respond(embed=embed)


@plurality.with_command
@tanjun.as_slash_command(
    "understanding_parts", "Learn how parts/alters view themselves, all about introjects, and substitute beliefs!"
)
async def understanding_parts(ctx: SlashContext):
    """Learn how parts/alters view themselves, all about introjects, and substitute beliefs!"""
    colors = [16758273, 2863491]
    color = choice(colors)  # noqa: S311
    description = (
        "BetterTogether provides 3 great pieces explaining multiple aspects "
        "of how Alters/Parts view themselves:\n\n"
        "[Part 1 goes over Belief and Appearance]"
        "(https://clever-and-unique-name.tumblr.com/post/188265286579/"
        "the-beginning-of-a-new-series-of-casual)"
        ', that coves questions like "why does an alter look like that?"\n\n'
        "[Part 2 talks about Introjects](https://clever-and-unique-name."
        "tumblr.com/post/188370998334/welcome-to-understanding-dissociated-parts-all)"
        ", how they form, why they form, and how to work with them.\n\n"
        "[Part 3 concludes with Substitute Belief](https://clever-and-unique-"
        "name.tumblr.com/post/613614699438850048/substitute-beliefs-is-an-umbrella-concept-that)"
        " and discusses how DID/OSDD Systems brain separates their memories "
        "into pieces, how that affects Parts, and how that influences their "
        "concept of self."
    )
    embed = hikari.Embed(title="Understanding Parts/Alters", description=description, color=color)
    await ctx.respond(embed=embed)


autism = faq.with_command(  # pylint: disable=C0103
    tanjun.slash_command_group("autism-spectrum-disorder", "Learn about ASD/Autism Spectrum Disorder!")
)


ASD_TERMS = {
    "comorbid": (
        'Oxford defines this as: "denoting or relating to diseases or medical '
        'conditions that are simultaneously present in a patient." ASD is often '
        "comorbid with OCD, SPD, Epilepsy, Gastro Disorders, Anxiety, Depression, and BPD"
    ),
    "special interest": (
        "Individuals with ASD often take one or more Special Interests (SI). "
        "These SI often manifest in intense and passion interests in a subject. "
        "The individual may lose themself in the topic to the degree they forget "
        "to eat, handle responsibilities, sleep, or even not noticing physical "
        "pain and further damaging the body."
        "\n"
        'While sometimes compared to an "intense nt hobby," SI\'s are often a '
        "requirement for individuals with ASD."
    ),
    "infodumping": (
        "Also called monologuing, is when an individual with ASD talks at length, "
        "usually about an SI. This can often come with a lack of awareness of other "
        "parties interest via body language and subtle reactions. [Keira Fulton-Lees]"
        "(https://medium.com/illumination-curated/the-autistic-trait-that-everyone-hates-1a4c725a0582)"
        " has a great post about InfoDumping on Medium."
    ),
    "meltdown": (
        "If an individual with ASD becomes overwhelmed by stress or a situation, may result"
        " in a meltdown. Meltdowns are entirely out of the individuals control and are no caused"
        " by anger or a temper tantrum as some imply. This loss of control may manifest in "
        "verbal outbursts like shouting, screaming and crying; physical outbursts like kicking, "
        "lashing out, or biting; or both."
    ),
    "shutdown": (
        "Inversely to a Meltdown which expresses and regulates emotions externally, a shutdown does"
        " so internally. Shutdowns are also caused by over stimulation or becoming overwhelmed. Shutdowns "
        "result in the individual going totally or mostly unresponsive. Just like Shutdowns, this is "
        "totally out of the individuals control."
    ),
    "echolalia": (
        "Echolalia causes individuals to repeat noises, sounds or phrases that they hear. ASD individuals "
        "can often develop Echolalia with verbal stimming as a self regulation method. Echolalia is very "
        "different from Tourettes Syndrome, which causes a person to to suddenly say or yell random "
        "things suddenly as a part of a tic."
    ),
    "stim": (
        "Self-Stimulatory behavior consists of repetitive actions or "
        "movements meant to help self regulate emotion "
        " or help alleviate stress. Stimming comes in "
        "many forms. It can be physical like spinning in circles, "
        '"hand flapping," or rocking. Stimming can also'
        " be vocal, the individual may repeat a noise over and over "
        "simply because it is a pleasing noise. \n\nMost stimming "
        "behavior is natural and out of the persons control."
    ),
    "spd": (
        "Sensory Processing Disorder (SPD), is a neurological "
        "condition in which sensory signals are not processed appropriately."
        " SPD may manifest in extreme sensitivity to sunlight,"
        " as the  brain perceives the sunlight, but processes the sensory"
        " input incorrectly. This results in the individual feeling "
        "more like a flashlight is being shone directly in their eye."
        " Because SPD is neurological it can can even cause the individuals intense pain."
        " SPD can also manifest with high aversions to specific food, textures, and fabrics."
    ),
    "nt": (
        'Oxford defines Neurotypical (NT) as "not displaying '
        "or characterized by autistic or other neurologically atypical patterns"
        'of thought or behavior." NT is a term widely used in '
        "the ASD community as a label for someone without ASD or any other"
        " neurological or mental disorders."
    ),
    "dyspraxia": (
        "Dyspraxia, also known as developmental coordination disorder (DCD),"
        " is a developmental disorder that causes problems with movement, "
        "coordination, judgment, processing, memory, and some other cognitive "
        "skills. It can also affect the body’s immune and nervous systems. "
        "Dyspraxia is fairly common and a very frequent comorbidity of Autism."
    ),
    "efd": (
        "Executive functions are a broad group of skills that enable people "
        "to complete tasks and interact with others. An Executive Function "
        "Disorder (EFD) can impair a person's ability to organize themselves "
        "and control their behavior--these include ADHD and ADD, though "
        "someone can be given a broad diagnosis of EFD."
    ),
    "ocd": (
        "Frequently misrepresented in media, Obsessive Compulsive Disorder is "
        "a disorder in which people have recurring, unwanted thoughts, ideas, "
        "or sensations (obsessions) that make them feel driven to do something "
        "repetitively (compulsions). This can range from worries about negative "
        "reactions from deviating from rigid routines to violent or otherwise "
        "unsavory intrusive thoughts or compulsions."
    ),
    "masking": (
        "Masking is a complex and costly survival strategy for Autistic people. "
        "It generally involves intentionally learning neurotypical behaviors and "
        "mimicking them in social situations. Though often learned naturally from "
        "a young age, this behavior can be incredibly damaging to Autistic people "
        "as their lives go on."
    ),
    "rsd": (
        "Rejection Sensitive Dysphoria (RSD) is a highly common symptom of EFD, "
        "specifically ADHD. This is generally a strong emotional reaction to "
        "perceived rejection, which can range from angry lashing out to outright "
        "panic. People who have the condition sometimes work hard to make everyone "
        "like and admire them, or they might stop trying and stay out of any "
        "situation where they might get hurt. "
    ),
}
ASD_TERMS_OPT = list(ASD_TERMS.keys())
ASD_TERMS_OPT.append("All")


@autism.with_command
@tanjun.with_str_slash_option(
    "term",
    "The term you want to look up (ex: meltdown) *Optional*.",
    choices=ASD_TERMS_OPT,
)
@tanjun.as_slash_command("terminology", "Learn some terminology common to Autism Spectrum Disorder (ASD)")
async def asd_terminology(ctx: SlashContext, term: str = "all") -> None:
    """Provides a search/filterable slash command about ASD terminology."""
    return_terms: dict = ASD_TERMS
    if term.lower() != "all":
        found_terms = ASD_TERMS.get(term.lower(), ASD_TERMS)

        if found_terms:
            return_terms = {term: found_terms}

    description = ""
    embed = hikari.Embed(title="Autism Spectrum Terminology", description=description)
    for found_term, found_def in return_terms.items():
        embed.add_field(name=found_term, value=found_def, inline=False)
    if guild := ctx.get_guild():
        embed.set_thumbnail(guild.icon_url)

    await ctx.respond(embed=embed)


@tanjun.as_loader
def load(client: tanjun.abc.Client) -> None:
    """Default tanun loader"""
    client.add_component(component.copy())


@tanjun.as_unloader
def unload(client: tanjun.Client, /) -> None:
    client.remove_component_by_name(component.name)
