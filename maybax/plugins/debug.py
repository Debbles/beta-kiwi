"""Debugger module only usable by bot owner."""
from __future__ import annotations

import datetime
import os
import platform
import time

import aioredis
import hikari
import psutil
import tanjun
from aioredis.exceptions import ResponseError
from tanjun.abc import SlashContext
from tanjun.checks import with_owner_check

component = tanjun.Component()

debug = component.with_slash_command(  # pylint: disable=C0103
    tanjun.slash_command_group("debug", "A bunch of debugger commands.")
)  # pylint: disable=C0103


@debug.add_command
@tanjun.with_str_slash_option(
    "clear_type", "The type of clear to use", choices=("terminal", "commands"), default="terminal"
)
@tanjun.as_slash_command("clear", "Clear terminal or commands")
async def clear_term(
    ctx: SlashContext, clear_type: str = "terminal", client: tanjun.Client = tanjun.injected(type=tanjun.Client)
):
    if clear_type == "terminal":
        if os.name == "nt":
            os.system("cls")  # noqa: S607, S605
        else:
            os.system("clear")  # noqa: S607, S605
        await ctx.respond("✅")
    elif clear_type == "commands":
        await client.clear_application_commands(guild=ctx.guild_id)
    else:
        await ctx.respond("Wow something went really wrong I'm so sorry.")
        return
    await ctx.respond("Cleared!")


@debug.with_command
@tanjun.as_slash_command("about", "About this bot.")
async def about_bot(ctx: SlashContext, process: psutil.Process = tanjun.cached_inject(psutil.Process)):
    """Get the bot's current delay."""
    bot_user = await ctx.rest.fetch_my_user()
    start_time = time.perf_counter()
    rest_latency = (time.perf_counter() - start_time) * 1_000
    gateway_latency = ctx.shards.heartbeat_latency * 1_000 if ctx.shards else float("NAN")
    start_date = datetime.datetime.fromtimestamp(process.create_time())
    uptime = datetime.datetime.now() - start_date
    memory_usage: float = process.memory_full_info().uss / 1024 ** 2
    cpu_usage: float = process.cpu_percent() / psutil.cpu_count()
    memory_percent: float = process.memory_percent()
    help_links = """[Support Website & Editor](https://maybax.patchwork.systems/maybax-commands/)\n
    [Maybax Commands Documentation & Help](https://maybax.patchwork.systems/maybax-commands/)\n
    [Support Discord](https://discord.gg/BeFQpxaMUm)\n
    [The Patchwork Collective Patreon (Maybax Developers)](https://www.patreon.com/patchworkcollective)\n
    [Gitlab Home](https://gitlab.com/aster.codes/maybax2)\n
    [Report an Issue](https://gitlab.com/aster.codes/maybax2/-/issues)\n
    """
    embed = (
        hikari.Embed()
        .set_thumbnail(bot_user.make_avatar_url())
        .set_author(name=f"About {bot_user.username}", url="https://gitlab.com/aster.codes/maybax2/")
        .add_field(name="Uptime", value=f"{uptime}", inline=True)
        .add_field(
            name="Memory Usage",
            value=f"{memory_usage:.2f} MB ({memory_percent:.0f}%)",
            inline=True,
        )
        .add_field(name="CPU Usage", value=f"{cpu_usage:.2f}% CPU", inline=True)
        .add_field(name="REST Client Ping:", value=f"{rest_latency}", inline=True)
        .add_field(name="Gateway Client Ping:", value=f"{gateway_latency}", inline=True)
        .add_field("Help & Support", value=help_links, inline=False)
        .set_footer(
            text=f"Made with Hikari v{hikari.__version__}, Tanjun {tanjun.__version__}, and Python {platform.python_version()}",
        )
    )

    await ctx.respond(embed=embed)


redis = debug.with_command(tanjun.slash_command_group("redis", "Redis debugging commands."))  # pylint: disable=C0103


@redis.with_command
@with_owner_check
@tanjun.with_str_slash_option("value", "The value to pass to the command.", default=None)
@tanjun.with_str_slash_option("key", "The Redis key to run in the command.", default=None)
@tanjun.with_str_slash_option("command", "The Redis command to run on the pool.")
@tanjun.as_slash_command("execute-command", "Run a command on Redis.")
async def execute_redis_command(
    ctx: SlashContext,
    command: str,
    key: str | None = None,
    value: str | None = None,
    redis_client: aioredis.Redis = tanjun.injected(type=aioredis.Redis),
):
    """Debugger command for redis.

    Parameters
    ==========
    command: str
        The Redis command to execute.
    key: str | None = None
        They key to provide the given Redis command.
    value: str | None = None
        They value to provide the given Redis command."""
    command = command.strip()
    if key:
        key = key.strip()
    if key and value:
        if "," in value:
            value_list = value.split(",")
            try:
                result = await redis_client.execute_command(command, key, *value_list)
            except ResponseError:
                value_list = [int(val) for val in value_list]
                result = await redis_client.execute_command(command, key, *value_list)

        else:
            result = await redis_client.execute_command(command, key, value)
    elif key and not value:
        result = await redis_client.execute_command(command, key)
    else:
        result = await redis_client.execute_command(command)

    if result is True:
        result = "***✅ Success!***"
    elif result is False:
        result = "***⛔ Returned False***"

    embed = hikari.Embed(
        title=f"Command `{command}` Results",
        description=f"{result}\n",
        color=14167072,
    ).set_author(name=ctx.author.username, icon=ctx.author.avatar_url)

    await ctx.respond(embed=embed)


@debug.with_command
@tanjun.with_str_slash_option("module_name", "The module to target.")
@tanjun.as_slash_command("reload_module", "Reloads a module.")
async def reload_module(
    ctx: tanjun.abc.SlashContext, module_name: str, client: tanjun.Client = tanjun.injected(type=tanjun.Client)
):
    """Reload a module in Tanjun"""
    try:
        client.reload_modules(module_name)
    except ValueError:
        client.load_modules(module_name)

    await client.declare_global_commands()
    await ctx.respond("Reloaded!")


@debug.with_command
@tanjun.with_str_slash_option("module_name", "The module to target.")
@tanjun.as_slash_command("unload_module", "Removes a module.")
async def unload_module(
    ctx: tanjun.abc.SlashContext, module_name: str, client: tanjun.Client = tanjun.injected(type=tanjun.Client)
):
    """Unload a module in Tanjun"""
    try:
        client.unload_modules(module_name)
    except ValueError:
        await ctx.respond("Couldn't unload module...")
        return

    await client.declare_global_commands()
    await ctx.respond("Unloaded!")


@debug.with_command
@tanjun.with_str_slash_option("module_name", "The module to reload.")
@tanjun.as_slash_command("load_module", "Loads a module.")
async def load_module(
    ctx: tanjun.abc.SlashContext, module_name: str, client: tanjun.Client = tanjun.injected(type=tanjun.Client)
):
    """Load a module in Tanjun"""
    try:
        client.load_modules(module_name)
    except ValueError:
        await ctx.respond("Can't find that module!")
        return

    await client.declare_global_commands()
    await ctx.respond("Loaded!")


@debug.with_command
@with_owner_check
@tanjun.with_member_slash_option(
    "member",
    "The member who's system cache you want to clear.",
)
@tanjun.as_slash_command("clear_system_cache", "Clear a specific members system cache.", default_to_ephemeral=True)
async def clear_system_cache(
    ctx: SlashContext,
    member: hikari.InteractionMember,
    redis: aioredis.Redis = tanjun.injected(type=aioredis.Redis),
):
    redis_key_system = f"{member.id}:system"
    redis_key_member_set = f"{member.id}:member_list"
    redis_key_group_set = f"{member.id}:group_list"
    if await redis.exists(redis_key_member_set):
        member_list = await redis.smembers(redis_key_member_set)
        for part_id in member_list:
            redis_key_member = f"{member.id}:{part_id}:member"
            await redis.delete(redis_key_member)
        await redis.delete(redis_key_member_set)
        await ctx.respond(f"Removed {member.mention} member cache!")
    if await redis.exists(redis_key_group_set):
        group_list = await redis.smembers(redis_key_group_set)
        for group_id in group_list:
            redis_key_group = f"{member.id}:{group_id}:group"
            await redis.delete(redis_key_group)
        await redis.delete(redis_key_group_set)
        await ctx.respond(f"Removed {member.mention} group cache!")
    if await redis.exists(redis_key_system):
        await redis.delete(redis_key_system)
        await ctx.respond(f"Removed {member.mention} system cache!")
    await ctx.respond("Cache cleanup done!")


@tanjun.as_loader
def load(client: tanjun.abc.Client) -> None:
    """Default tanjun loader"""
    client.add_component(component.copy())


@tanjun.as_unloader
def unload(client: tanjun.Client, /) -> None:
    client.remove_component_by_name(component.name)
