"""Provides some quick templates of embeds for the St Jude charity event!"""
from __future__ import annotations

import hikari
import tanjun
from tanjun.abc import SlashContext

INCENTIVES = [
    {
        "title": "Pet Photo",
        "details": (
            "Your donation will earn you a photo of a desired staff's pet! \nPet Choices:\n-"
            " Boi (Emod's Cat)\n- Fen (Patchwork's Dog)\n- Iris (Patchwork's Dog)\n- Pepper "
            "(Deb's Dog)"
        ),
        "price": 2,
    },
    {
        "title": "GTD Twitter Roast",
        "details": "Your donation will result in @GameTheoryD roasting you on twitter.",
        "price": 7,
    },
    {
        "title": "GTD Pet Video",
        "details": (
            "Your donation will earn you a video of a desired staff's pet! \nPet Choices:\n-"
            " Boi (Emod's Cat)\n- Fen (Patchwork's Dog)\n- Iris (Patchwork's Dog)\n- Pepper "
            "(Deb's Dog)"
        ),
        "price": 1,
    },
    {
        "title": "GTD Twitter Shoutout",
        "details": "Your donation will result in @GameTheoryD giving you a shoutout on twitter!",
        "price": 15,
    },
    {
        "title": "GT Cast Twitter Shoutout",
        "details": "Tyler Mascola, editor of Game Theory, will give you a shoutout on Twitter too!",
        "price": 20,
    },
    {
        "title": "GTD Requestable Pet Photo/Video",
        "details": (
            "Your donation will earn you a photo of a desired staff's pet or video. "
            "Want it with a funny hat or doing a trick? This is the option to pick. \n"
            "Pet Choices:\n- Boi (Emod's Cat)\n- Fen (Patchwork's Dog)\n- Iris "
            "(Patchwork's Dog)\n- Pepper (Deb's Dog)"
        ),
        "price": 20,
    },
]

INCENTIVE_ROLES = [
    {
        "title": "Donator",
        "mention": "<@&893133774391476245>",
        "image": "https://cdn.discordapp.com/attachments/882388078763859998/892905906914852944/St-Jude-Base-Badge-PNG.png",
        "price": 1,
    },
    {
        "title": "Generous Donator",
        "mention": "<@&893133901072068640>",
        "image": "https://cdn.discordapp.com/attachments/882388078763859998/892905906914852944/St-Jude-Base-Badge-PNG.png",
        "price": 10,
    },
    {
        "title": "Big-Hearted Donator",
        "mention": "<@&893133963982434334>",
        "image": "https://cdn.discordapp.com/attachments/882388078763859998/892905910937202728/St-Jude-Silver-Badge-PNG.png",
        "price": 25,
    },
    {
        "title": "Hero Donator",
        "mention": "<@&893134007678664756>",
        "image": "https://cdn.discordapp.com/attachments/882388078763859998/892905914401710140/St-Jude-Gold-Badge-PNG.png",
        "price": 50,
    },
    {
        "title": "Nice! Donator",
        "mention": "<@&893134058333286482>",
        "image": "https://cdn.discordapp.com/attachments/882388078763859998/892905914401710140/St-Jude-Gold-Badge-PNG.png",
        "price": 69,
    },
]

component = tanjun.Component()

st_jude = component.with_slash_command(  # pylint: disable=C0103
    tanjun.slash_command_group("st_jude", "Quick St Jude campaign Help")
)


@st_jude.with_command
@tanjun.with_int_slash_option("donation", "The amount the member donated.")
@tanjun.with_member_slash_option("member", "The member who donated")
@tanjun.as_slash_command("donation-embed", "Generate a  Donation Embed.")
async def donation_embed(ctx: SlashContext, donation: int, member: hikari.InteractionMember):
    """Generates an embed that displays the members name, donation amount and earned rewards!"""
    title = f":tada: {member.username}#{member.discriminator} just donated {donation} :tada:"
    desc = f"{member.mention} just donated ${donation} to The Game Theory Community St Jude Charity Drive! Thanks so much!!"
    dono_embed = hikari.Embed(title=title, description=desc, color=16766720)

    for incentive in INCENTIVES:
        if incentive["price"] <= donation:
            dono_embed.add_field(name=incentive["title"], value=incentive["details"])
    selected_roles = []
    for role in INCENTIVE_ROLES:
        if role["price"] <= donation:
            selected_roles.append(role)

    dono_embed.add_field(
        name="Donator Role", value=f"You will receive the {selected_roles[-1]['title']} role until 2022!"
    )
    dono_embed.set_thumbnail(selected_roles[-1]["image"])

    dono_embed.add_field(
        name="Keep up with the Campaign!",
        value="[Campaign Page](https://tiltify.com/@the-patchwork-collective/game-theory-discord-community-drive)",
        inline=True,
    )
    dono_embed.add_field(
        name="Donate now!",
        value="[Click Here](https://tiltify.com/@the-patchwork-collective/game-theory-discord-community-drive/donate)",
        inline=True,
    )

    await ctx.respond(embed=dono_embed)


@tanjun.as_loader
def load(client: tanjun.abc.Client) -> None:
    """Default tanun loader"""
    client.add_component(component.copy())


@tanjun.as_unloader
def unload(client: tanjun.Client, /) -> None:
    client.remove_component_by_name(component.name)
