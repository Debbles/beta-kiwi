from __future__ import annotations

import datetime
import json
import logging
from io import BytesIO

import aiopg
import aioredis
import hikari
import pytz
import tanjun
from checks.system_checks import has_local_guild_system_role, has_registered_system
from models.front.system import SystemModel
from tanjun import SlashContext
from tanjun.checks import with_check, with_owner_check

logger = logging.getLogger(__name__)

component = tanjun.Component()

FMT = "%m/%d/%Y %I:%M:%S %Z"
ALLOWED_PROXY_KEY = "{guild_id}:allowed_proxy_role_id"

system_group = component.with_slash_command(  # pylint: disable=C0103
    tanjun.slash_command_group("system", "Edit and perform system actions.", default_to_ephemeral=True)
)


@system_group.with_command
@with_check(has_registered_system)
@with_check(has_local_guild_system_role)
@tanjun.with_str_slash_option("system_name", "The new name of your System.", default="")
@tanjun.with_str_slash_option(
    "description", "The new description for your System. (Displays in the Introduction Embed)", default=""
)
@tanjun.with_str_slash_option("system_tag", "The new SystemTag of your System.", default="")
@tanjun.with_str_slash_option(
    "system_picture", "This picture will display as the thumbnail on your System Intro Embed.", default=""
)
@tanjun.with_str_slash_option("pluralkit_id", "Provide a PluralKit ID for easy sync'ing.", default="")
@tanjun.with_str_slash_option("tupperbox_id", "Provide a Tupperbox ID for easy sync'ing.", default="")
@tanjun.as_slash_command("edit", "Display information about my System.")
async def edit(
    ctx: SlashContext,
    system_name: str = "",
    description: str = "",
    system_tag: str = "",
    system_picture: str = "",
    pluralkit_id: str = "",
    tupperbox_id: str = "",
    system_model: SystemModel = tanjun.injected(type=SystemModel),
    pg_pool: aiopg.Pool = tanjun.injected(type=aiopg.Pool),
    redis: aioredis.Redis = tanjun.injected(type=aioredis.Redis),
):
    system_model = await system_model.setup(ctx.member.id, pg_pool, redis)
    if not system_model:
        await ctx.respond("Sorry! It doesn't look like you have a registered system to this discord account...")
        return
    if system_name:
        system_model.name = system_name
    if description:
        system_model.description = description
    if system_tag:
        system_model.tag = system_tag
    if system_picture:
        system_model.avatar_url = system_picture
    if pluralkit_id:
        system_model.pluralkit_id = pluralkit_id
    if tupperbox_id:
        system_model.tupper_id = int(tupperbox_id)

    if system_name or description or system_tag or system_picture or pluralkit_id or tupperbox_id:
        await system_model.save_to_postgres(pg_pool)
        await system_model.cache(ctx.member.id, redis)
        await ctx.respond(f"Updated {system_model.name}!")
    else:
        await ctx.respond(f"It doesn't look like you provided anything to update on {system_model.name}!")


@system_group.with_command
@with_check(has_registered_system)
@with_check(has_local_guild_system_role)
@tanjun.with_bool_slash_option("show", "Display your System information (without ID's and TZ) for everyone.")
@tanjun.as_slash_command("my-system", "Display information about my System.")
async def my_system(
    ctx: SlashContext,
    show: bool = False,
    system_model: SystemModel = tanjun.injected(type=SystemModel),
    pg_pool: aiopg.Pool = tanjun.injected(type=aiopg.Pool),
    redis: aioredis.Redis = tanjun.injected(type=aioredis.Redis),
):
    system_model = await system_model.setup(ctx.member.id, pg_pool, redis)
    if not system_model:
        await ctx.respond("Sorry! It doesn't look like you have a registered system to this discord account...")
        return
    embed = (
        hikari.Embed(
            title=f"{system_model.name}",
            description=f"{system_model.description}",
            color=system_model.color if hasattr(system_model, "color") and system_model.color else 000,
        )
        # .set_thumbnail(system_model.avatar_url if system_model.avatar_url else "")
        .set_author(name=f"{ctx.author.username}'s System", icon=ctx.author.avatar_url)
        .add_field(name="System Tag:", value=f"{system_model.tag if system_model.tag else '-'}")
        .set_footer(text=f"Owner ID: {system_model.owner_discord_id}", icon=ctx.get_guild().icon_url)
    )
    if not show:
        (
            embed.add_field(name="ID:", value=f"{system_model.id_}", inline=True)
            .add_field(
                name="Pluralkit ID:",
                value=f"{system_model.pluralkit_id if system_model.pluralkit_id else '[ Not Connected ]'}",
                inline=True,
            )
            .add_field(
                name="Tupper ID:",
                value=f"{system_model.tupper_id if system_model.tupper_id else '[ Not Connected ]'}",
                inline=True,
            )
            .add_field(
                name="Member Count:",
                value=f"{len(system_model.members) if system_model.members else '[ No Members ]'}",
                inline=True,
            )
            .add_field(
                name="Group Count:",
                value=f"{len(system_model.groups) if system_model.groups else '[ No Groups ]'}",
                inline=True,
            )
            .add_field(name="Timezone:", value=f"{system_model.timezone}")
        )
    ctx.set_ephemeral_default(not show)
    await ctx.respond(content="Your System:", embed=embed)


@system_group.with_command
@with_check(has_local_guild_system_role)
@tanjun.with_str_slash_option("system_name", "The Systems Collective name.", default="")
@tanjun.as_slash_command("register", "Register this account as a System.")
async def register(
    ctx: SlashContext,
    system_name: str,
    system_model: SystemModel = tanjun.injected(type=SystemModel),
    pg_pool: aiopg.Pool = tanjun.injected(type=aiopg.Pool),
    redis: aioredis.Redis = tanjun.injected(type=aioredis.Redis),
):
    if not system_name:
        system_name = f"{ctx.author.username}'s System"
    system_model = SystemModel(
        name=system_name,
        owner_discord_id=str(ctx.author.id),
        created_at=datetime.datetime.now().astimezone(pytz.timezone("US/Eastern")),
    )
    system_model = await system_model.save_to_postgres(pg_pool)
    await system_model.cache(ctx.author.id, redis)
    await ctx.respond(f"System {system_name} registered for {ctx.author.mention}", user_mentions=True)


@system_group.with_command
@with_owner_check
@tanjun.as_slash_command(
    "save-system", "Save the current in memory collective to a file/db.", default_to_ephemeral=True
)
async def save_system(
    ctx: SlashContext,
    patchwork: SystemModel = tanjun.injected(type=SystemModel),
    pg_pool: aiopg.Pool = tanjun.injected(type=aiopg.Pool),
    redis: aioredis.Redis = tanjun.injected(type=aioredis.Redis),
) -> None:
    """Helper function to save current PatchworkMember and PatchworkGroup to json files."""
    patchwork = await patchwork.setup(ctx.member.id, pg_pool, redis)
    if not patchwork:
        await ctx.respond("Sorry! It doesn't look like you have a registered system to this discord account...")
        return
    await patchwork.save_to_postgres(pg_pool)
    await ctx.edit_initial_response(content="Written!")


@system_group.with_command
@with_check(has_registered_system)
@with_check(has_local_guild_system_role)
@tanjun.with_str_slash_option(
    "export_type",
    "The kind of Export file you want.",
    choices=[
        "PluralKit",
        "TupperBox",
    ],
    default="PluralKit",
)
@tanjun.as_slash_command("export", "Export to a PluralKit or TupperBox JSON file.", default_to_ephemeral=True)
async def export(
    ctx: SlashContext,
    export_type: str = "PluralKit",
    system_model: SystemModel = tanjun.injected(type=SystemModel),
    pg_pool: aiopg.Pool = tanjun.injected(type=aiopg.Pool),
    redis: aioredis.Redis = tanjun.injected(type=aioredis.Redis),
):
    system_model = await system_model.setup(ctx.member.id, pg_pool, redis)
    if not system_model:
        await ctx.respond("Sorry! It doesn't look like you have a registered system to this discord account...")
        return
    prefix = ""
    json_file = BytesIO()
    json_system = {}
    if export_type == "TupperBox":
        prefix = "tupper-"
        json_system["tuppers"] = []
        json_system["groups"] = []
        if system_model.members:
            for member in system_model.members:
                member_dict = member.to_tupper().to_dict()
                member_dict["id"] = member_dict["id_"]
                del member_dict["id_"]
                json_system["tuppers"].append(member_dict)
        if system_model.groups:
            for group in system_model.groups:
                group_dict = group.to_tupperbox().to_dict()
                group_dict["id"] = group_dict["id_"]
                del group_dict["id_"]
                json_system["groups"].append(group_dict)
    else:
        prefix = "pluralkit-"
        if system_model.pluralkit_id:
            await ctx.create_initial_response("Pluralkit export starting!")
        else:
            await ctx.create_initial_response("You do not have a Pluralkit ID saved for your system! Please add one!")
            return
        json_system["version"] = 1
        json_system["id"] = system_model.pluralkit_id
        json_system["name"] = system_model.name
        json_system["description"] = system_model.description or None
        json_system["tag"] = system_model.tag or None
        json_system["avatar_url"] = system_model.avatar_url or None
        json_system["timezone"] = system_model.timezone
        json_system["created"] = system_model.created_at.isoformat()
        json_system["accounts"] = {0: str(ctx.author.id)}
        json_system["members"] = []

        if system_model.members:
            for member in system_model.members:
                member_dict = member.to_pluralkit().to_dict()
                member_dict["id"] = member_dict["id_"]
                del member_dict["id_"]
                json_system["members"].append(member_dict)

    json_file.write(json.dumps(json_system, sort_keys=True).encode())
    json_file.seek(0)

    await ctx.edit_initial_response(
        "Here's your file!", attachment=hikari.Bytes(json_file.getvalue(), f"{prefix}-system.json")
    )


@tanjun.as_loader
def load(client: tanjun.abc.Client) -> None:
    """Default tanjun loader"""
    client.add_component(component.copy())


@tanjun.as_unloader
def unload(client: tanjun.Client, /) -> None:
    client.remove_component_by_name(component.name)
