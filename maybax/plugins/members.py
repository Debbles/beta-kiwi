from __future__ import annotations

import asyncio
import datetime
import logging
from functools import partial

import aiopg
import aioredis
import hikari
import httpx
import tanjun
import yuyo
from checks.system_checks import has_local_guild_system_role, has_registered_system
from hikari.messages import ButtonStyle
from models.front.members import PatchworkMember, PluralKitMember, TupperMember
from models.front.system import SystemModel
from tanjun.abc import SlashContext
from tanjun.checks import with_check

logger = logging.getLogger(__name__)

component = tanjun.Component()

members_group = component.with_slash_command(  # pylint: disable=C0103
    tanjun.slash_command_group("members", "Actions to work with your Systems Members.", default_to_ephemeral=True)
)


@members_group.with_command
@with_check(has_registered_system)
@with_check(has_local_guild_system_role)
@tanjun.with_bool_slash_option("show", "Make visible to other people. (default False)", default=False)
@tanjun.with_int_slash_option("part_id", "The ID of the Part you want to edit.", default=None)
@tanjun.as_slash_command("show_member", "Edit a Part in the System.", default_to_ephemeral=True)
async def show_member(
    ctx: SlashContext,
    part_id: int | None = None,
    show: bool = False,
    system_model: SystemModel = tanjun.injected(type=SystemModel),
    pg_pool: aiopg.Pool = tanjun.injected(type=aiopg.Pool),
    redis: aioredis.Redis = tanjun.injected(type=aioredis.Redis),
    component_client: yuyo.ComponentClient = tanjun.injected(type=yuyo.ComponentClient),
):
    system_model = await system_model.setup(ctx.member.id, pg_pool, redis)
    if not system_model:
        await ctx.respond("Sorry! It doesn't look like you have a registered system to this discord account...")
        return
    if not part_id:
        message = await ctx.respond(
            content="Click or Press the menu below to select a System Member.",
            component=system_model.select_fronting_menu(ctx),
            ensure_result=True,
        )

        executor = yuyo.components.WaitFor(authors=(ctx.author.id,), timeout=datetime.timedelta(seconds=60))
        component_client.set_executor(message, executor)

        try:
            result = await executor.wait_for()
        except asyncio.TimeoutError:
            await ctx.respond("Timeout! Please select a Part to front within 60 seconds.")
            return
        else:
            part_id = int(result.interaction.values[0])

    select_member = system_model.get_part_by_id(part_id)
    if not select_member:
        await ctx.respond(f"Sorry, I couldn't find a Part with the ID of `{part_id}`.")
        return
    ctx.set_ephemeral_default(not show)
    await ctx.respond(select_member.embed(select_member, ctx=ctx, show=show))


@members_group.with_command
@with_check(has_registered_system)
@with_check(has_local_guild_system_role)
@tanjun.with_str_slash_option("name", "The new name of the Part.", default="")
@tanjun.with_str_slash_option("tag", "The new tag for the Part.", default="")
@tanjun.with_str_slash_option("display_name", "The new display_name for the Part.", default="")
@tanjun.with_str_slash_option("pronouns", "The new pronouns for the Part.", default="")
@tanjun.with_str_slash_option("color", "The new color for the Part.", default="")
@tanjun.with_str_slash_option("avatar_url", "The new avatar_url for the Part.", default="")
@tanjun.with_str_slash_option("birthday", "The new birthday for the Part.", default="")
@tanjun.with_str_slash_option("description", "The new description for the Part.", default="")
@tanjun.with_int_slash_option("tupper_id", "The new Tupperbox ID for the Part.", default=None)
@tanjun.with_str_slash_option("pluralkit_id", "The new PluralKit ID for the Part.", default="")
@tanjun.with_int_slash_option("part_id", "The ID of the Part you want to edit.")
@tanjun.as_slash_command("edit", "Edit a Part in the System.", default_to_ephemeral=True)
async def edit(
    ctx: SlashContext,
    part_id: int,
    name: str = "",
    tag: str = "",
    display_name: str = "",
    pronouns: str = "",
    color: str = "",
    avatar_url: str = "",
    birthday: str = "",
    description: str = "",
    tupper_id: int | None = None,
    pluralkit_id: str = "",
    pg_pool: aiopg.Pool = tanjun.injected(type=aiopg.Pool),
    redis: aioredis.Redis = tanjun.injected(type=aioredis.Redis),
    system_model: SystemModel = tanjun.injected(type=SystemModel),
):
    """Edit a Part of the System."""
    system_model = await system_model.setup(ctx.member.id, pg_pool, redis)
    if not system_model:
        await ctx.respond("Sorry! It doesn't look like you have a registered system to this discord account...")
        return
    editing_member = None
    for member in system_model.members:
        if part_id == int(member.id_):
            editing_member = member
    if not editing_member:
        await ctx.respond(f"Sorry, I couldn't find a Part with the ID of `{part_id}`.")
        return
    if name:
        editing_member.name = name
    if tag:
        editing_member.tag = tag
    if display_name:
        editing_member.display_name = display_name
    if pronouns:
        editing_member.pronouns = pronouns
    if color:
        editing_member.color = color
    if avatar_url:
        editing_member.avatar_url = avatar_url
    if birthday:
        editing_member.birthday = birthday
    if description:
        editing_member.description = description
    if tupper_id:
        editing_member.tupper_id = tupper_id
    if pluralkit_id:
        editing_member.pluralkit_id = pluralkit_id
    if (
        name
        or tag
        or display_name
        or pronouns
        or color
        or avatar_url
        or birthday
        or description
        or tupper_id
        or pluralkit_id
    ):
        await editing_member.save_to_postgres(system_model.id_, pg_pool)
        await editing_member.cache(ctx.member.id, redis)
        await ctx.respond(f"Saved info for {editing_member.name}!")
    else:
        await ctx.respond("It doesn't look like you provided anything to update!")


@members_group.with_command
@with_check(has_registered_system)
@with_check(has_local_guild_system_role)
@tanjun.as_slash_command("list", "List all Parts in the System.", default_to_ephemeral=True)
async def list_(
    ctx: SlashContext,
    component_client: yuyo.ComponentClient = tanjun.injected(type=yuyo.ComponentClient),
    pg_pool: aiopg.Pool = tanjun.injected(type=aiopg.Pool),
    redis: aioredis.Redis = tanjun.injected(type=aioredis.Redis),
    patchwork: SystemModel = tanjun.injected(type=SystemModel),
):
    """List all of the System Parts."""
    patchwork = await patchwork.setup(ctx.member.id, pg_pool, redis)
    if not patchwork:
        await ctx.respond("Sorry! It doesn't look like you have a registered system to this discord account...")
        return
    iterator = (
        (
            f"{part.name} (Page {count+1}/{len(patchwork.members)})",
            PatchworkMember.embed(part, ctx=ctx, show=False),
        )
        for count, part in enumerate(patchwork.members)
    )
    paginator = yuyo.ComponentPaginator(iterator, authors=(ctx.author,), timeout=datetime.timedelta(minutes=3))

    if first_response := await paginator.get_next_entry():
        content, embed = first_response
        message = await ctx.edit_initial_response(content=content, component=paginator, embed=embed)
        component_client.set_executor(message, paginator)
        return
    else:
        await ctx.respond("No members in your system!")


@members_group.with_command
@with_check(has_registered_system)
@with_check(has_local_guild_system_role)
@tanjun.with_str_slash_option("import_url", "URL for json file from pluralkit or tupperbox to import.")
@tanjun.as_slash_command("import", "Import members from a Tupperbox/PluralKit Export URL", default_to_ephemeral=True)
async def import_(
    ctx: SlashContext,
    import_url: str,
    httpx_client: httpx.AsyncClient = tanjun.injected(type=httpx.AsyncClient),
    component_client: yuyo.ComponentClient = tanjun.injected(type=yuyo.ComponentClient),
    pg_pool: aiopg.Pool = tanjun.injected(type=aiopg.Pool),
    redis: aioredis.Redis = tanjun.injected(type=aioredis.Redis),
    patchwork: SystemModel = tanjun.injected(type=SystemModel),
) -> None:
    """Slash command to import either Tupperbox or Pluralkit JSON Export into PatchworkMember."""
    patchwork = await patchwork.setup(ctx.member.id, pg_pool, redis)
    if not patchwork:
        await ctx.respond("Sorry! It doesn't look like you have a registered system to this discord account...")
        return
    await ctx.defer()
    try:
        json_resp = await httpx_client.get(import_url)
        json_con = json_resp.json()
        await ctx.respond("We were able to download the file and read it!")

    except (httpx.HTTPStatusError, httpx.RequestError, httpx.HTTPError):
        await ctx.edit_initial_response(content="There was an error downloading your file!")
        return

    if json_con.get("members"):
        logging.debug("Received PK import")
        await new_import_pluralkit(ctx, json_con, component_client, patchwork, pg_pool, redis)

    elif json_con.get("tuppers"):
        logging.debug("Received Tupper import")
        await new_import_tupperbox(ctx, json_con, component_client, patchwork, pg_pool, redis)


@tanjun.as_loader
def load(client: tanjun.abc.Client) -> None:
    """Default tanjun loader"""
    client.add_component(component.copy())


@tanjun.as_unloader
def unload(client: tanjun.Client, /) -> None:
    client.remove_component_by_name(component.name)


""" Helper Functions """


async def new_import_pluralkit(
    ctx: SlashContext,
    pk_json: dict,
    component_client: yuyo.ComponentClient,
    system_model: SystemModel,
    pg_pool: aiopg.Pool,
    redis: aioredis.Redis,
):
    """Helper function to handle pluralkit json imports, start to finish."""
    system_model = await system_model.setup(ctx.member.id, pg_pool, redis)
    if not system_model:
        await ctx.respond("Sorry! It doesn't look like you have a registered system to this discord account...")
        return
    await ctx.edit_initial_response(content="Looks like a Pluralkit file! Let's get started!")
    iterator = (
        (
            f"{part['name']} (Page {count+1}/{len(pk_json.get('members'))})",
            hikari.Embed(
                title=f"{part['name']}",
                description=part["description"] if part["description"] else "-",
                color=part["color"] if part["color"] else "000000",
            )
            .set_thumbnail(part["avatar_url"])
            .add_field(
                name="Brackets:",
                value="\n".join([f"Prefix: {tag['prefix']}\nSuffix: {tag['suffix']}" for tag in part["proxy_tags"]])
                if part["proxy_tags"]
                else "[ None Set ]",
                inline=True,
            )
            .add_field(name="Created At:", value=part.get("created", "-"), inline=True)
            .add_field(name="Pronouns:", value=part["pronouns"] if part["pronouns"] else "-", inline=True)
            .add_field(name="Nickname:", value=part["display_name"] if part["display_name"] else "-", inline=True)
            .add_field(name="Birthday:", value=part["birthday"] if part["birthday"] else "-", inline=True)
            .set_author(name=ctx.member.username, icon=ctx.member.avatar_url)
            .set_footer(text=(f"User ID: {part['id'] if part['id'] else '-'}")),
        )
        for count, part in enumerate(pk_json.get("members", []))
    )
    select_part_with_json_callback = partial(select_part_pk_partial, pk_json, system_model, redis, pg_pool)
    paginator = (
        yuyo.ComponentPaginator(iterator, authors=(ctx.author,), timeout=datetime.timedelta(minutes=3))
        .add_button(ButtonStyle.PRIMARY, select_part_with_json_callback)
        .set_emoji("✔️")
        .set_label("Accept Changes")
        .add_to_container()
    )
    if first_response := await paginator.get_next_entry():
        content, embed = first_response
        message = await ctx.edit_initial_response(content=content, component=paginator, embed=embed)
        component_client.set_executor(message, paginator)
        return
    await ctx.respond(":(")


async def select_part_pk_partial(
    pk_json: dict, patchwork: SystemModel, redis: aioredis.Redis, pg_pool: aiopg.Pool, ctx: yuyo.ComponentContext
):
    """Callback function for the confirm/save Pluralkit import."""
    matched = False
    db_save = False
    match_embed = ctx.interaction.message.embeds[0]
    await ctx.defer(hikari.ResponseType.DEFERRED_MESSAGE_CREATE)
    for part in pk_json.get("members", []):
        if match_embed.title.lower() == part.get("name", "").lower():
            part["id_"] = part["id"]
            del part["id"]
            patch_part = PluralKitMember(**part)
            break

    for saved_part in patchwork.members:
        if patch_part.id_ == saved_part.pluralkit_id or patch_part.name.lower() == saved_part.name.lower():
            saved_part.update_from_pluralkit(patch_part)
            matched = True

    if not matched:
        saved_part = PatchworkMember.from_pluralkit(patch_part)
        await patchwork.add_member(saved_part, redis)

    if patchwork.id_:
        saved_part = await saved_part.save_to_postgres(patchwork.id_, pg_pool)
        db_save = True

    if db_save:
        await saved_part.cache(pk_json["accounts"][0], redis)
        message = f"Saved member {patch_part.name} to {patchwork.name}!"
    else:
        message = "Cannot save {patch_part.name} because you do not have System Registered!"

    confirm_message = await ctx.respond(message, embed=match_embed)
    await asyncio.sleep(2)
    await confirm_message.delete()


async def new_import_tupperbox(
    ctx: SlashContext,
    tp_json: dict,
    component_client: yuyo.ComponentClient,
    system_model: SystemModel,
    pg_pool: aiopg.Pool,
    redis: aioredis.Redis,
):
    """Helper function to handle tupperbox json imports, start to finish."""
    system_model = await system_model.setup(ctx.member.id, pg_pool, redis)
    if not system_model:
        await ctx.respond("Sorry! It doesn't look like you have a registered system to this discord account...")
        return
    await ctx.edit_initial_response(content="Looks like a Tupperbox file! Let's get started!")
    iterator = (
        (
            f"{part['name']} (Page {count+1}/{len(tp_json.get('tuppers'))})",
            hikari.Embed(title=f"{part['name']}", description=part["description"] if part["description"] else "-")
            .set_thumbnail(part["avatar_url"])
            .add_field(name="Created At:", value=part.get("created_at", "-"), inline=True)
            .add_field(name="Birthday:", value=part["birthday"] if part["birthday"] else "-", inline=True)
            .add_field(name="Nick:", value=part["nick"] if part["nick"] else "-", inline=True)
            .add_field(name="Tag:", value=part["tag"] if part["tag"] else "-", inline=True)
            .add_field(name="Brackets:", value="\n".join(filter(None, part["brackets"])), inline=True)
            .set_author(name=ctx.member.username, icon=ctx.member.avatar_url)
            .set_footer(
                text=(
                    f"User ID: {part['user_id'] if part['user_id'] else '-'} | "
                    f"Group ID: {part['group_id'] if part['group_id'] else '-'}"
                )
            ),
        )
        for count, part in enumerate(tp_json.get("tuppers", []))
    )
    select_part_with_json_callback = partial(select_part_tupper_partial, tp_json, system_model, redis, pg_pool)
    paginator = (
        yuyo.ComponentPaginator(iterator, authors=(ctx.author,), timeout=datetime.timedelta(minutes=3))
        .add_button(ButtonStyle.PRIMARY, select_part_with_json_callback)
        .set_emoji("✔️")
        .set_label("Accept Changes")
        .add_to_container()
    )
    if first_response := await paginator.get_next_entry():
        content, embed = first_response
        message = await ctx.edit_initial_response(content=content, component=paginator, embed=embed)
        component_client.set_executor(message, paginator)
        return
    await ctx.respond(":(")


async def select_part_tupper_partial(
    tp_json: dict, patchwork: SystemModel, redis: aioredis.Redis, pg_pool: aiopg.Pool, ctx: yuyo.ComponentContext
):
    """Callback for the confirm/save on Tupper Import"""
    matched = False
    db_save = False
    match_embed = ctx.interaction.message.embeds[0]
    await ctx.defer(hikari.ResponseType.DEFERRED_MESSAGE_CREATE)
    for part in tp_json.get("tuppers", []):
        if match_embed.title.lower() == part.get("name", "").lower():
            part["id_"] = part["id"]
            del part["id"]
            patch_part = TupperMember(**part)
            break

    for saved_part in patchwork.members:
        if patch_part.id_ == saved_part.tupper_id or patch_part.name.lower() == saved_part.name.lower():
            saved_part.update_from_tupperbox(patch_part)
            matched = True

    if not matched:
        saved_part = PatchworkMember.from_tupper(patch_part)
        await patchwork.add_member(saved_part, redis)

    if patchwork.id_:
        saved_part = await saved_part.save_to_postgres(patchwork.id_, pg_pool)
        db_save = True

    if db_save:
        await saved_part.cache(tp_json["tuppers"][0]["user_id"], redis)
        confirm = f"Saved member {patch_part.name} to {patchwork.name}!"
    else:
        confirm = f"Cannot save {patch_part.name} because no system is saved to DB"
    confirm_message = await ctx.respond(confirm, embed=match_embed)
    await asyncio.sleep(2)
    await confirm_message.delete()
