from __future__ import annotations

import asyncio
import datetime
import logging
from functools import partial

import aiopg
import aioredis
import hikari
import httpx
import tanjun
import yuyo
from checks.system_checks import has_local_guild_system_role, has_registered_system
from hikari.messages import ButtonStyle
from models.front.groups import PatchworkGroup, PluralKitGroup, TupperGroup
from models.front.system import SystemModel
from tanjun.abc import SlashContext
from tanjun.checks import with_check

logger = logging.getLogger(__name__)

component = tanjun.Component()

groups_group = component.with_slash_command(  # pylint: disable=C0103
    tanjun.slash_command_group("groups", "Edit and perform system actions.", default_to_ephemeral=True)
)


@groups_group.with_command
@with_check(has_registered_system)
@with_check(has_local_guild_system_role)
@tanjun.with_str_slash_option("import_url", "URL for json file from pluralkit or tupperbox to import.")
@tanjun.as_slash_command("import", "Import groups from a Tupperbox/PluralKit Export URL", default_to_ephemeral=True)
async def import_(
    ctx: SlashContext,
    import_url: str,
    httpx_client: httpx.AsyncClient = tanjun.injected(type=httpx.AsyncClient),
    component_client: yuyo.ComponentClient = tanjun.injected(type=yuyo.ComponentClient),
    pg_pool: aiopg.Pool = tanjun.injected(type=aiopg.Pool),
    redis: aioredis.Redis = tanjun.injected(type=aioredis.Redis),
    patchwork: SystemModel = tanjun.injected(type=SystemModel),
) -> None:
    """Slash command to import either Tupperbox ro Pluralkit JSON Export into PatchworkGroup"""
    patchwork = await patchwork.setup(ctx.member.id, pg_pool, redis)
    if not patchwork:
        await ctx.respond("Sorry! It doesn't look like you have a registered system to this discord account...")
        return
    await ctx.defer()
    try:
        json_resp = await httpx_client.get(import_url)
        json_con = json_resp.json()
        await ctx.respond("We were able to download the file and read it!")

    except (httpx.HTTPStatusError, httpx.RequestError, httpx.HTTPError):
        await ctx.edit_initial_response(content="There was an error downloading your file!")
        return

    if json_con.get("members"):
        logging.debug("Received PK import")
        await update_patchwork_group_from_pk(ctx, json_con, patchwork, pg_pool, redis)

    elif json_con.get("tuppers"):
        logging.debug("Received Tupper import")
        await update_patchwork_group_from_tupper(ctx, json_con, component_client, patchwork, pg_pool, redis)


@groups_group.with_command
@with_check(has_registered_system)
@with_check(has_local_guild_system_role)
@tanjun.as_slash_command("list", "List all Groups in the System.", default_to_ephemeral=True)
async def list_(
    ctx: SlashContext,
    component_client: yuyo.ComponentClient = tanjun.injected(type=yuyo.ComponentClient),
    pg_pool: aiopg.Pool = tanjun.injected(type=aiopg.Pool),
    redis: aioredis.Redis = tanjun.injected(type=aioredis.Redis),
    patchwork: SystemModel = tanjun.injected(type=SystemModel),
):
    """List all of the System Groups."""
    patchwork = await patchwork.setup(ctx.member.id, pg_pool, redis)
    if not patchwork:
        await ctx.respond("Sorry! It doesn't look like you have a registered system to this discord account...")
        return
    iterator = (
        (
            f"{group.name} (Page {count+1}/{len(patchwork.groups)})",
            PatchworkGroup.embed(group, ctx=ctx, show=False),
        )
        for count, group in enumerate(patchwork.groups)
    )
    paginator = yuyo.ComponentPaginator(iterator, authors=(ctx.author,), timeout=datetime.timedelta(minutes=3))

    if first_response := await paginator.get_next_entry():
        content, embed = first_response
        message = await ctx.edit_initial_response(content=content, component=paginator, embed=embed)
        component_client.set_executor(message, paginator)
        return
    else:
        await ctx.respond("No groups in your system!")


@tanjun.as_loader
def load(client: tanjun.abc.Client) -> None:
    """Default tanjun loader"""
    client.add_component(component.copy())


@tanjun.as_unloader
def unload(client: tanjun.Client, /) -> None:
    client.remove_component_by_name(component.name)


""" Helper Functions """


async def update_patchwork_group_from_pk(
    ctx: SlashContext,
    pk_json: dict,
    system_model: SystemModel,
    pg_pool: aiopg.Pool,
    redis: aioredis.Redis,
):
    """Helper function to update PatchworkGroup from a pluralkit json export."""
    pk_group = PluralKitGroup(
        id_=pk_json["id"],
        name=pk_json["name"],
        description=pk_json.get("description"),
        tag=pk_json.get("tag"),
        avatar_url=pk_json.get("avatar_url"),
    )
    system_model.name = pk_group.name
    system_model.description = pk_group.description
    system_model.tag = pk_group.tag
    system_model.avatar_url = pk_group.avatar_url
    system_model.timezone = pk_group.timezone
    system_model.pluralkit_id = pk_group.id_
    system_model = await system_model.save_to_postgres(pg_pool)
    await system_model.cache(int(system_model.owner_discord_id), redis)
    await ctx.respond(f"Updated {system_model.name} from Pluralkit ID {pk_group.id_}")


async def update_patchwork_group_from_tupper(
    ctx: SlashContext,
    tp_json: dict,
    component_client: yuyo.ComponentClient,
    system_model: SystemModel,
    pg_pool: aiopg.Pool,
    redis: aioredis.Redis,
):
    """Helper function to update PatchworkGroup from a tupper json export."""

    await ctx.edit_initial_response(content="Looks like a TupperBox file! Let's get started!")
    iterator = (
        (
            f"{group['name']} (Page {count+1}/{len(tp_json.get('groups'))})",
            hikari.Embed(
                title=f"{group['name']}",
                description=group["description"] if group["description"] else "-",
            )
            .add_field(name="Tag:", value=group["tag"] if group["tag"] else "-", inline=True)
            .add_field(name="Tupper ID:", value=group["id"] if group["id"] else "-", inline=True)
            .add_field(name="Tupper Position:", value=group["position"] if group["position"] else "-", inline=True)
            .set_footer((f"User ID: {group['user_id'] if group['user_id'] else '-'}")),
        )
        for count, group in enumerate(tp_json.get("groups", []))
    )
    select_part_with_json_callback = partial(select_group_tp_partial, tp_json, system_model, pg_pool, redis)
    paginator = (
        yuyo.ComponentPaginator(iterator, authors=(ctx.author,), timeout=datetime.timedelta(minutes=3))
        .add_button(ButtonStyle.PRIMARY, select_part_with_json_callback)
        .set_emoji("✔️")
        .set_label("Accept Changes")
        .add_to_container()
    )
    if first_response := await paginator.get_next_entry():
        content, embed = first_response
        message = await ctx.edit_initial_response(content=content, component=paginator, embed=embed)
        component_client.set_executor(message, paginator)
        return


async def select_group_tp_partial(
    tp_json: dict, system_model: SystemModel, pg_pool: aiopg.Pool, redis: aioredis.Redis, ctx: yuyo.ComponentContext
):
    """Callback for the confirm/save on Tupper Group Import"""
    matched_key = None
    tup_group = None
    match_embed = ctx.interaction.message.embeds[0]
    await ctx.defer(hikari.ResponseType.DEFERRED_MESSAGE_CREATE)

    for group in tp_json.get("groups", []):
        if match_embed.title.lower() == group.get("name", "").lower():
            group["id_"] = group["id"]
            del group["id"]
            tup_group = TupperGroup(**group)
            break

    if tup_group.id_ == system_model.id_ or tup_group.name.lower == system_model.name.lower():
        system_model.name = tup_group.name
        system_model.description = tup_group.description
        system_model.tag = tup_group.tag
        system_model = await system_model.save_to_postgres(pg_pool)
        await system_model.cache(int(system_model.owner_discord_id), redis)
        matched_key = system_model

    if not matched_key:
        for saved_group in system_model.groups:
            if tup_group.name.lower() == saved_group.name.lower() or tup_group.id_ == saved_group.id_:
                matched_key = saved_group

    if matched_key and tup_group:
        if isinstance(matched_key, PatchworkGroup):
            matched_key.update_from_tupperbox(tup_group)
    else:
        matched_key = PatchworkGroup.from_tupperbox(tup_group)

    matched_key = await matched_key.save_to_postgres(system_model.id_, pg_pool)
    if matched_key.id_:
        await matched_key.cache(int(system_model.owner_discord_id), redis)

    confirm_message = await ctx.respond(f"Saved Group {tup_group.name} to {system_model.name}!", embed=match_embed)
    await asyncio.sleep(2)
    await confirm_message.delete()
