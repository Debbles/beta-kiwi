"""Provides simple grounding exercises."""
from __future__ import annotations

from random import choice

import hikari
import tanjun
from tanjun import SlashContext

component = tanjun.Component()

grounding_exercise = component.with_slash_command(  # pylint: disable=C0103
    tanjun.slash_command_group("grounding-exercise", "Get help with grounding exercises!")
)


@grounding_exercise.with_command
@tanjun.as_slash_command("support-hotlines", "Provides support numbers and hotlines.")
async def support_hotlines(
    ctx: SlashContext, bot: hikari.GatewayBotAware = tanjun.injected(type=hikari.GatewayBotAware)
):
    embed = (
        hikari.Embed(title="Support Hotlines")
        .add_field(
            name="USA National Suicide Hotlines:",
            value=(
                "[National Suicide Prevention Lifeline]"
                "(https://suicidepreventionlifeline.org/)\n"
                "(800) 273-8255\n\n"
                "[Nacional de Prevención del Suicidio]"
                "(https://suicidepreventionlifeline.org/help-yourself/en-espanol/)\n"
                "(888) 628-9454\n\n"
                "[National Suicide Prevention Lifeline (Options for Deaf and Hard of Hearing)]"
                "(https://suicidepreventionlifeline.org/help-yourself/for-deaf-hard-of-hearing/)"
                "\n"
                "For TTY Users: Use your preferred relay service or dial 711 then 1-800-273-8255"
            ),
            inline=True,
        )
        .add_field(
            name="USA National Hotlines",
            value=(
                "[National Domestic Violence Hotline]"
                "(https://www.thehotline.org/)\n"
                "(800) 799-7233\n\n"
                "[National Grad Crisis Line]"
                "(https://gradresources.org/crisis/)\n"
                "(877) 472-3457\n\n"
                "[National Sexual Assault Hotline]"
                "(https://www.rainn.org/)\n"
                "(800) 656-4673\n\n"
                "[Childhelp National Child Abuse Hotline]"
                "(https://childhelphotline.org/)\n"
                "(800) 422-4453\n\n"
                "[CDC National HIV and AIDS Hotline]"
                "(https://www.cdc.gov/hiv/library/hotlines.html)\n"
                "(800) 232-4636\n\n"
                "[Substance Abuse and Mental Health Services Administration National Helpline]"
                "(https://www.samhsa.gov/find-help/national-helpline)\n"
                "(800) 662-4357"
            ),
            inline=True,
        )
        .add_field(
            name="LGBTQ Support",
            value=(
                "[The Trevor Project]"
                "(http://www.thetrevorproject.org/)\n"
                "866.488.7386\n\n"
                "[The Network la Red]"
                "(http://tnlr.org/)\n"
                "617.742.4911\n\n"
                "[National Coalition of Anti-Violence Programs]"
                "(http://www.avp.org/)\n"
                "212.714.1141\n\n"
                "[GLBT National Hotline]"
                "(http://www.glbtnationalhelpcenter.org/)\n"
                "888.THE.GLNH \n"
                "(843.4564)\n\n"
                "[FORGE](http://www.forge-forward.org/index.php)\n-----\n"
            ),
            inline=True,
        )
        .add_field(
            name="College Students",
            value=(
                "[NotAlone.gov]"
                "(http://www.notalone.gov/)\n\n"
                "[Know Your IX]"
                "(http://knowyourix.org/)"
                "[End Rape on Campus]"
                "(http://www.endrapeoncampus.org/)"
            ),
            inline=True,
        )
        .add_field(
            name="Mental Health",
            value=(
                "[Sidran Traumatic Stress Foundation]"
                "(http://www.sidran.org/)\n\n"
                "[GoodTherapy.org]"
                "(http://www.goodtherapy.org/)\n\n"
                "[National Eating Disorder Helpline]"
                "(http://www.nationaleatingdisorders.org/)\n\n"
                "[National Alliance on Mental Illness (NAMI)]"
                "(http://www.nami.org/)"
            ),
            inline=True,
        )
        .add_field(
            name="International Hotlines:",
            value=(
                "[International Suicide Hotlines]"
                "(http://www.suicide.org/international-suicide-hotlines.html)\n"
                "[International Emergency Numbers:]"
                "(https://thelifelinecanada.ca/help/crisis-centres/worldwide-emergency-numbers/)\n"
                "[International Crisis Centers:]"
                "(https://thelifelinecanada.ca/help/crisis-centres/international-crisis-centres/)\n"
                "[International Emergency Numbers:]"
                "(https://thelifelinecanada.ca/help/crisis-centres/worldwide-emergency-numbers/)"
            ),
            inline=False,
        )
        .add_field(
            name="Disabilities Support",
            value="[National Disability Rights Network](http://www.ndrn.org/index.php)",
            inline=False,
        )
        .add_field(
            name="Sources",
            value=(
                "[RAINN National Resources]"
                "(https://www.rainn.org/national-resources-sexual-assault-survivors-and-their-loved-ones)"
            ),
            inline=False,
        )
        .set_thumbnail(bot.get_me().avatar_url)
    )

    await ctx.respond(embed)


@grounding_exercise.with_command
@tanjun.as_slash_command("other-grounding-methods", "Provides other commonly used grounding techniques.")
async def other_grounding_methods(
    ctx: SlashContext, bot: hikari.GatewayBotAware = tanjun.injected(type=hikari.GatewayBotAware)
):
    embed = (
        hikari.Embed(title="Grounding Methods")
        .add_field(
            name="Hot/Cold",
            value=(
                "Put your hands in or under warm water. Notice how the water feels, it's temperature "
                "and how it feels on your skin. Focus on the sensation on your fingertips, palms, and "
                "back of the hands. Does it feels the same on every part of the hand?"
            ),
            inline=False,
        )
        .add_field(
            name="Breathing Exercises",
            value=(
                "Breath slowly and deeply, pausing briefly between each breath."
                "Slow, methodical and consistent is most important. "
                "/grounding-exercise breathing-exercises` provides timed gifs to help"
                " keep a stable pattern."
            ),
            inline=False,
        )
        .add_field(
            name="Move your Body",
            value=(
                "Do a few stretches, go for a walk, do a couple jumping jacks, bounce up and down, "
                "anything that stretches and flexes muscles will help! Try to focus on how you body"
                " feels. Maybe it's the pressure of the floor against your feet, or the air bustling "
                "around your fingers as your hands move. If there's a consistent sound can you focus "
                " on it? Like the soft *thwak* of a jump rope or the methodic thumping of jumping jacks."
            ),
            inline=False,
        )
        .add_field(
            name="5-4-3-2-1 Method",
            value=(
                "This method is similar to the one described in `//grounding-exercise coping-with-trauma-dissoc-method `."
                "\n\n Start by noticing 5 things you can *see* around you. Describe those 5 items. What shape are they? "
                "Color? Size? Do they look textured?\n\n"
                "Next identify 4 things you can *touch*. Similar to before, describe how each item feels. "
                "Maybe temperature, wet, dry, etc."
                "\n\n"
                "Listen for 3 thing you can *hear* and try to describe the sounds.\n\n"
                "Identify 2 *smells*.\n\n"
                "Lastly try to identify 1 thing that you can *taste*."
            ),
            inline=False,
        )
        .add_field(
            name="Memory Games",
            value=(
                "Any game that makes you recall from memory helps grounding tremendously. "
                "Simple card matching games, trying to redraw a picture from memory, or even"
                " flashcards!"
            ),
            inline=False,
        )
        .add_field(
            name="Categorize",
            value=(
                "Pick one or two broad categories and spend a few minutes listing as many items as you can that belong in "
                "those categories."
            ),
            inline=False,
        )
        .add_field(
            name="Math & Numbers",
            value=(
                "Counting by sets: 2, 4, 6, 8... or 8, 16, 24, 32... or 10, 20, 30...\n"
                "Counting backwards from 100.\n"
                "Pick a number (e.x.: 25) and think of 5 ways to make that number (e.x.: 5 * 5 = 25, 15 + 10 = 25...)"
            ),
            inline=False,
        )
        .add_field(
            name="Recite from Memory",
            value=(
                "If know any comforting scripts, poems, quotes, or book passages say them quietly to yourself."
                " Focus on the muscles in your mouth and tongue. How you shape each sound. If you are more visual, "
                "try closing your eyes and imagining writing each word as you say it as text on a page."
            ),
            inline=False,
        )
        .add_field(
            name="Use an Anchoring Phrase",
            value=(
                "Example: 'My name is Full Name. I am X years old. Today is June 2nd, 2021 at 4pm.' You can extend"
                " these phrases with more information too. Things like other people in the room or weather can also help."
            ),
            inline=False,
        )
        .add_field(
            name="Describe your Surroundings",
            value=(
                "Try to describe your immediate surrounds in as much detail as possible. Just like with 5-4-3-2-1, use all"
                " your sense."
            ),
            inline=False,
        )
        .set_author(name=ctx.member.username, icon=ctx.member.avatar_url)
        .set_thumbnail(bot.get_me().avatar_url)
    )
    await ctx.respond(embed)


@grounding_exercise.with_command
@tanjun.as_slash_command("breathing-exercises", "Provides GIFs with basic breathing exercise guides for grounding.")
async def breathing_exercises(
    ctx: SlashContext, bot: hikari.GatewayBotAware = tanjun.injected(type=hikari.GatewayBotAware)
):
    """"""
    grounding_gifs = [
        "https://c.tenor.com/oMaE0M-_MUsAAAAd/meditation-you-got-this.gif",
        "https://c.tenor.com/HYF3BhrI9-IAAAAd/breathe-out-breathe-in.gif",
        "https://c.tenor.com/A7tOUVDPUN4AAAAd/infinity-breath.gif",
        "https://c.tenor.com/EJ9eQOoQAGAAAAAC/breathing-technique.gif",
        "https://c.tenor.com/zTEwa0rrFJ4AAAAd/breathe-inhale.gif",
        "https://c.tenor.com/cEENHFvqhtYAAAAd/breathing-exercise.gif",
    ]
    description = (
        "Breathing exercises are all about the slow rhythm. Use the gif below and try to match your breathing with it."
    )
    embed = (
        hikari.Embed(title="Breathing Exercise", description=description)
        .set_image(choice(grounding_gifs))  # noqa: S311
        .set_author(name=ctx.member.username, icon=ctx.member.avatar_url)
        .set_thumbnail(bot.get_me().avatar_url)
    )
    await ctx.respond(embed)


@grounding_exercise.with_command
@tanjun.as_slash_command(
    "coping-with-trauma-dissoc-method",
    "Learn about a grounding exercise from Coping with Trauma-Related Dissociation",
)
async def coping_with_trauma_related_dissociation(  # noqa: CFQ001
    ctx: SlashContext, bot: hikari.GatewayBotAware = tanjun.injected(type=hikari.GatewayBotAware)
):
    """
    Provides an embed with simple grounding exercises from Coping with Trauma-Related
    Dissociation.
    """
    description = (
        "[Coping with Trauma-Related Dissociation](https://www.amazon.com/Coping-Trauma-Related-"
        "Dissociation-Interpersonal-Neurobiology/dp/039370646X/ref=pd_sbs_1/143-8050920-8375535) "
        "provides a break down of a great explanation of grounding exercises that anyone can do "
        "anywhere. \n\n We have provided a word-for-word copy of this process below:"
    )
    embed = (
        hikari.Embed(title="Learning to be Present, a Grounding Exercise", description=description)
        .add_field(
            name="Visual Grounding",
            value=(
                "Notice five objects that you see in the room and pay close attention "
                "to their details (shape, color, texture, size, etc.). Make sure you do"
                " not hurry through this part of the exercise. Let your eyes linger over"
                " each object. Name three characteristics of the object out loud to "
                "yourself, for example, 'It is blue. It is big. It is round.'"
            ),
            inline=False,
        )
        .add_field(
            name="Audio Grounding",
            value=(
                "Notice four sounds that you hear in the present (inside or outside of"
                " the room). Listen to their quality. Are they loud or soft, constant or"
                " intermittent, pleasant or unpleasant? Again, name three characteristics "
                "of the sound out loud to yourself, for example, 'It is loud, grating, and"
                " definitely unpleasant.'"
            ),
            inline=False,
        )
        .add_field(
            name="Tactile Grounding",
            value=(
                "Now touch three objects close to you and describe out loud to yourself"
                " how they feel, for example, rough, smooth, cold, warm, hard or soft, "
                "and so forth."
            ),
            inline=False,
        )
        .add_field(
            name="Repeat",
            value=(
                "Return to the three objects that you have chosen to observe with your"
                " eyes. As you notice them, concentrate on the fact that you are here "
                "and now with these objects in the present, in this room. Next, notice "
                "the sounds and concentrate on the fact that you are here in this room "
                "with those sounds. Finally, do the same with  the objects you have "
                "touched. You can expand this exercise by repeating it several times, "
                "three items for each sense, then two for each, then one, and then build "
                "it up again to three. You can also add new items to keep your practice fresh."
            ),
            inline=False,
        )
        .add_field(name="Examples", value="------", inline=False)
        .add_field(
            name="Sight:",
            value=(
                "Look around the room for something (or even someone) that can help"
                " remind you that you are in the present, for example, a piece of clothing"
                " you are wearing that you like, a particular color or shape or texture,"
                " a picture on the wall, a small object, a book. Name the object to "
                "yourself out loud."
            ),
            inline=False,
        )
        .add_field(
            name="Sound:",
            value=(
                "Use the sounds around you to help you really focus on the here and now."
                " For example, listen to the normal everyday noises around you: the heat "
                "or air conditioning or refrigerator running, people talking, doors"
                " opening or closing, traffic sounds, birds singing, a fan blowing. "
                "You can remind yourself: 'These are the sounds of normal life all around "
                "me. I am safe. I am here.'"
            ),
            inline=False,
        )
        .add_field(
            name="Taste:",
            value=(
                "Carry a small item of food with you that has a pleasant but"
                " intense taste, for example, lozenges, mints, hard candy or "
                "gum, a piece of fruit such as an orange or banana. If you feel"
                " ungrounded,pop it into your mouth and focus on the flavor and"
                " the feel of it in your mouth to help you be more here and now."
            ),
            inline=False,
        )
        .add_field(
            name="Smell:",
            value=(
                "Carry something small with you that has a pleasant smell, for "
                "example, a favorite hand lotion, perfume, aftershave, or an "
                "aromatic fruit such as an orange. When you start to feel spacey"
                " or otherwise not very present, a pleasant smell is a powerful"
                " reminder of the present."
            ),
            inline=False,
        )
        .add_field(
            name="Touch:",
            value=(
                "Try one or more of the following touch exercises that feels good to "
                "you. Touch the chair or sofa on which you are sitting, or your clothes."
                " Feel them with your fingers and be very aware of the textures and weight"
                " of the fabric. Try pushing on the floor with your feet, so that you "
                "can really feel the floor supporting you. Squeeze your hands together and"
                " let the pressure and warmth remind you that you are here and now. Press your "
                "tongue hard to the roof of your mouth. Cross your arms over your chest with"
                " your fingertips on your collar bones and pat your chest, alternating left and"
                " right, reminding yourself that you are in the present and safe (the butterfly "
                "hug, Artigas & Jarero, 2005)."
            ),
            inline=False,
        )
        .add_field(
            name="Breathing",
            value=(
                "The way in which we breathe is crucial in helping us to be present. "
                "When people dissociate or space out, they are usually breathing very "
                "shallowly and rapidly or hold their breath too long. Take time to "
                "slow and regulate your breathing. Breathe in through your nose to "
                "a slow count of three, hold to the count of three, and then breathe"
                " out through your mouth to a slow count of three. Do this several "
                "times while being mindful of how you breathe. Notice whether there "
                "are already ways in which you ground yourself in the present."
            ),
            inline=False,
        )
        .set_author(name=ctx.member.username, icon=ctx.member.avatar_url)
        .set_thumbnail(bot.get_me().avatar_url)
    )
    await ctx.respond(embed=embed)


@tanjun.as_loader
def load_examples(client: tanjun.abc.Client) -> None:
    """Default tanjun loader."""
    client.add_component(component.copy())


@tanjun.as_unloader
def unload(client: tanjun.Client, /) -> None:
    client.remove_component_by_name(component.name)
