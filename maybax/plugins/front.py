"""Module to handle Patchwork Collective fronting."""
from __future__ import annotations

import asyncio
import datetime

import aiopg
import aioredis
import hikari
import tanjun
import yuyo
from checks.system_checks import has_local_guild_system_role, has_registered_system
from models.front.members import PatchworkMember
from models.front.system import SystemModel
from tanjun import SlashContext
from tanjun.checks import with_check

component = tanjun.Component()

fronting_group = component.with_slash_command(  # pylint: disable=C0103
    tanjun.slash_command_group("front", "Set status for fronting and auto.", default_to_ephemeral=True)
)


@fronting_group.with_command
@with_check(has_registered_system)
@with_check(has_local_guild_system_role)
@tanjun.as_slash_command("set-fronting", "Set what Part/Alter is fronting for proxies.", default_to_ephemeral=True)
async def set_fronting(
    ctx: SlashContext,
    system_model: SystemModel = tanjun.injected(type=SystemModel),
    pg_pool: aiopg.Pool = tanjun.injected(type=aiopg.Pool),
    redis: aioredis.Redis = tanjun.injected(type=aioredis.Redis),
    component_client: yuyo.ComponentClient = tanjun.injected(type=yuyo.ComponentClient),
) -> None:
    """Slash command to set enabled, whether bot should proxy."""
    system_model = await system_model.setup(ctx.member.id, pg_pool, redis)
    if not system_model:
        await ctx.respond("Sorry! It doesn't look like you have a registered system to this discord account...")
        return
    message = await ctx.respond(
        content="Click or Press the menu below to select a System Member.",
        component=system_model.select_fronting_menu(ctx),
        ensure_result=True,
    )

    executor = yuyo.components.WaitFor(authors=(ctx.author.id,), timeout=datetime.timedelta(seconds=60))
    component_client.set_executor(message, executor)

    try:
        result = await executor.wait_for()
    except asyncio.TimeoutError:
        await ctx.respond("Timeout! Please select a Part to front within 60 seconds.")
    else:
        member = system_model.get_part_by_id(int(result.interaction.values[0]))
        if not member:
            await ctx.respond("Something broke, oh no!")
            return
        system_model.fronting = member
        await system_model.cache(ctx.member.id, redis)
        await ctx.respond(
            content=f"Set {member.name} to the Front!",
            embed=PatchworkMember.embed(member, ctx=ctx, show=False),
        )


@fronting_group.with_command
@with_check(has_registered_system)
@with_check(has_local_guild_system_role)
@tanjun.with_str_slash_option("status", "Enable or Disable", choices=["Enabled", "Disable"])
@tanjun.as_slash_command("enabled", "Enable/Disable Proxying! (Patchwork only)", default_to_ephemeral=True)
async def fronting_enabled(
    ctx: SlashContext,
    status: str,
    system_model: SystemModel = tanjun.injected(type=SystemModel),
    pg_pool: aiopg.Pool = tanjun.injected(type=aiopg.Pool),
    redis: aioredis.Redis = tanjun.injected(type=aioredis.Redis),
) -> None:
    """Slash command to set enabled, whether bot should proxy."""
    system_model = await system_model.setup(ctx.member.id, pg_pool, redis)
    if not system_model:
        await ctx.respond("Sorry! It doesn't look like you have a registered system to this discord account...")
        return

    if status == "Enabled":
        system_model.enabled = True
    else:
        system_model.enabled = False
    await system_model.cache(ctx.member.id, redis)
    await ctx.respond(f"Proxy status updated: {status}")


@fronting_group.with_command
@with_check(has_registered_system)
@with_check(has_local_guild_system_role)
@tanjun.as_slash_command("check-front-status", "See your current Fronting status", default_to_ephemeral=True)
async def check_front_status(
    ctx: SlashContext,
    system_model: SystemModel = tanjun.injected(type=SystemModel),
    pg_pool: aiopg.Pool = tanjun.injected(type=aiopg.Pool),
    redis: aioredis.Redis = tanjun.injected(type=aioredis.Redis),
    bot: hikari.GatewayBotAware = tanjun.injected(type=hikari.GatewayBotAware),
) -> None:
    """Slash command to display enabled, auto and fronting."""
    system_model = await system_model.setup(ctx.member.id, pg_pool, redis)
    if not system_model:
        await ctx.respond("Sorry! It doesn't look like you have a registered system to this discord account...")
        return
    bot_user = await bot.get_me().fetch_self()
    embed = (
        hikari.Embed(title=f"Fronting Status for {system_model.name}")
        .add_field(name="Proxy Enabled:", value=f"{'☑️' if system_model.enabled else '❌'}", inline=True)
        .add_field(name="Auto Proxy:", value=f"{'☑️' if system_model.auto else '❌'}", inline=True)
        .set_thumbnail(bot_user.make_avatar_url(ext="png"))
        .set_author(name=ctx.author.username, icon=ctx.author.make_avatar_url(ext="png"))
    )
    if system_model.fronting:
        (
            embed.add_field(name="Fronting Member(s):", value=f"{system_model.fronting.name}", inline=False)
            .add_field(name="Fronting Indicator:", value=f"{system_model.fronting.indicators}", inline=False)
            .set_thumbnail(system_model.fronting.avatar_url)
        )

    await ctx.respond(embed)


@fronting_group.with_command
@with_check(has_registered_system)
@with_check(has_local_guild_system_role)
@tanjun.with_str_slash_option("auto_status", "The status to set.", choices=["sticky", "None", "off"])
@tanjun.as_slash_command(
    "auto",
    "Set how the bot should handle fronting. (stick | None | off)",
    default_to_ephemeral=True,
)
async def set_auto(
    ctx: SlashContext,
    auto_status: str,
    system_model: SystemModel = tanjun.injected(type=SystemModel),
    pg_pool: aiopg.Pool = tanjun.injected(type=aiopg.Pool),
    redis: aioredis.Redis = tanjun.injected(type=aioredis.Redis),
) -> None:
    """Slash command to update the `auto` proxy status.

    - Sticky - continue to implicitly use last front until otherwise Specified.
    - None/off - require front indicator for proxy"""
    system_model = await system_model.setup(ctx.member.id, pg_pool, redis)
    if not system_model:
        await ctx.respond("Sorry! It doesn't look like you have a registered system to this discord account...")
        return
    system_model.auto = True if auto_status.lower() == "sticky" else False
    await system_model.cache(ctx.member.id, redis)
    await ctx.respond(f"Set fronting auto status to {auto_status}")


@tanjun.as_loader
def load(client: tanjun.abc.Client) -> None:
    """Default tanjun loader"""
    client.add_component(component.copy())


@tanjun.as_unloader
def unload(client: tanjun.Client, /) -> None:
    client.remove_component_by_name(component.name)
