"""Module provides 2 listeners for message proxying.

proxy_message_update_event
    Listens for GuildMessageUpdateEvent and runs process_patchwork_proxy

proxy_message_event
    Listens for GuildMessageCreateEvent and runs process_patchwork_proxy"""
from __future__ import annotations

import logging
import os

import aiopg
import aioredis
import emoji
import hikari
import tanjun
from hikari import GuildMessageCreateEvent, GuildMessageUpdateEvent
from hikari.webhooks import IncomingWebhook
from models.front.members import PatchworkMember
from models.front.system import SystemModel
from tanjun.abc import SlashContext
from tanjun.checks import with_owner_check

HOOK_EMOJI = "🪝"
ALLOWED_PROXY_KEY = "{guild_id}:allowed_proxy_role_id"


component = tanjun.Component()
logger = logging.getLogger(__name__)


@component.with_command
@with_owner_check
@tanjun.with_str_slash_option("content", "Content to update to.")
@tanjun.as_slash_command("edit-last-message", "Edit a proxied message.", default_to_ephemeral=True)
async def edit_last_message(
    ctx: SlashContext,
    content: str,
    system_model: SystemModel = tanjun.injected(type=SystemModel),
    pg_pool: aiopg.Pool = tanjun.injected(type=aiopg.Pool),
    redis: aioredis.Redis = tanjun.injected(type=aioredis.Redis),
) -> None:
    """Edit a proxy message."""
    system_model = await system_model.setup(ctx.member.id, pg_pool, redis)
    if not system_model:
        await ctx.respond("Sorry! It doesn't look like you have a registered system to this discord account...")
        return
    guild = ctx.get_guild()
    channel = guild.get_channel(ctx.channel_id)
    await ctx.respond("🎚️ Checking for Channel")
    if not channel:
        return
    await ctx.edit_initial_response("✉️ Checking for Message")
    last_message, *_ = (
        await channel.fetch_history()
        .filter(lambda message: hasattr(message, "webhook_id") and message.webhook_id is not None)
        .limit(1)
    )
    if not last_message.webhook_id:
        return
    await ctx.edit_initial_response(f"{HOOK_EMOJI} Checking for Hook")
    webhook: hikari.ExecutableWebhook = await ctx.rest.fetch_webhook(last_message.webhook_id)

    if content:
        await ctx.edit_initial_response("✒️ Updating Author!")
        await webhook.edit_message(message=last_message, content=content)
    await ctx.edit_initial_response("✅ Message updated!")


@component.with_command
@with_owner_check
@tanjun.with_str_slash_option("content", "Content to update to.")
@tanjun.with_str_slash_option("message_id", "The ID of the Message you want to edit.")
@tanjun.as_slash_command("edit-proxy-message", "Edit a proxied message.", default_to_ephemeral=True)
async def edit_message(
    ctx: SlashContext,
    message_id: int,
    content: str,
    system_model: SystemModel = tanjun.injected(type=SystemModel),
    pg_pool: aiopg.Pool = tanjun.injected(type=aiopg.Pool),
    redis: aioredis.Redis = tanjun.injected(type=aioredis.Redis),
) -> None:
    """Edit a proxy message."""
    system_model = await system_model.setup(ctx.member.id, pg_pool, redis)
    if not system_model:
        await ctx.respond("Sorry! It doesn't look like you have a registered system to this discord account...")
        return
    channel = ctx.get_channel()
    await ctx.respond("🎚️ Checking for Channel")
    if not channel:
        return
    message = await channel.fetch_message(message_id)
    await ctx.edit_initial_response("✉️ Checking for Message")
    if not message.webhook_id:
        return
    webhook = await ctx.rest.fetch_webhook(message.webhook_id)
    await ctx.edit_initial_response(f"{HOOK_EMOJI} Checking for Hook")
    await webhook.edit_message(message=message, content=content)
    await ctx.edit_initial_response("✅ Message edited!")


@component.with_listener(GuildMessageUpdateEvent)
async def proxy_message_update_event(
    event: GuildMessageUpdateEvent,
    system_model: SystemModel = tanjun.injected(type=SystemModel),
    redis: aioredis.Redis = tanjun.injected(type=aioredis.Redis),
    pg_pool: aiopg.Pool = tanjun.injected(type=aiopg.Pool),
):
    """Listener for message edits to start proxy."""
    if not system_model or not event.member or event.member.is_bot:
        return
    guild_key = ALLOWED_PROXY_KEY.format(guild_id=event.guild_id)
    if await redis.exists(guild_key) == 0:
        return

    proxy_role_id = await redis.get(guild_key)
    if int(proxy_role_id) in set(event.member.role_ids):
        await process_patchwork_proxy(event, patchwork=system_model, redis=redis)


@component.with_listener(hikari.GuildMessageCreateEvent)
async def proxy_message_event(
    event: hikari.GuildMessageCreateEvent,
    system_model: SystemModel = tanjun.injected(type=SystemModel),
    pg_pool: aiopg.Pool = tanjun.injected(type=aiopg.Pool),
    redis: aioredis.Redis = tanjun.injected(type=aioredis.Redis),
):
    """Listener for message send to start proxy."""
    system_model = await system_model.setup(event.author.id, pg_pool, redis)

    if not system_model or not event.member or event.member.is_bot:
        return

    guild_key = ALLOWED_PROXY_KEY.format(guild_id=event.guild_id)
    if await redis.exists(guild_key) == 0:
        return

    proxy_role_id = await redis.get(guild_key)
    if int(proxy_role_id) in set(event.member.role_ids):
        await process_patchwork_proxy(event, patchwork=system_model, redis=redis)


async def process_patchwork_proxy(
    event: GuildMessageCreateEvent | GuildMessageUpdateEvent, patchwork: SystemModel, redis: aioredis.Redis
):
    """Main handler for proxying.

    Only allows for Owner via id currently. If the Owner message contains a
    proxy indicator from PatchworkDep and system_status['enabled'] is True
    this function will run to completion and  proxy the owners message.
    """
    if not patchwork.enabled or event.is_bot or not event.content:
        return

    member, matched_tag = await process_message(event.content, patchwork=patchwork)
    message_content = ""
    if not member and patchwork.auto and patchwork.fronting:
        message_content = event.content if event.content is not None else "-"
    elif matched_tag:
        patchwork.fronting = member
        await patchwork.cache(event.member.id, redis)
        tagless_message = event.content.split(matched_tag.strip())
        if tagless_message[0]:
            message_content = tagless_message[0]
        else:
            message_content = tagless_message[1]
    else:
        return

    await webhook_send(event, patchwork.fronting, message_content, patchwork=patchwork)


async def process_message(event: str, patchwork: SystemModel) -> list[PatchworkMember, str] | list[None]:
    """Help function to check if an incoming message matches a possible proxy member."""
    for member in patchwork.members:
        if member.indicators:
            for indicator in member.indicators:
                if (
                    indicator.get("suffix", None)
                    and indicator.get("suffix", "").strip() != ""
                    and event.endswith(indicator.get("suffix").strip())
                ):
                    return [
                        member,
                        indicator["suffix"],
                    ]
                if (
                    indicator.get("prefix", None)
                    and indicator.get("prefix").strip() != ""
                    and event.startswith(indicator.get("prefix").strip())
                ):
                    return [
                        member,
                        indicator["prefix"],
                    ]
    return [
        None,
        None,
    ]


async def webhook_send(
    event: GuildMessageCreateEvent | GuildMessageUpdateEvent,
    member: PatchworkMember,
    message: hikari.UndefinedOr[str],
    patchwork: SystemModel,
):
    """
    Helper function to send a proxy a message via webhook.

    Currently supports:
    - pfp
    - member name
    - auto add | Patchwork Collective
    - message
    - replies
    - embed_message
    - attachments
    """
    webhook = await get_webhooks(event)
    tag = ""
    if member.tag:
        tag = member.tag
    elif patchwork.tag:
        tag = patchwork.tag
    data = {
        "webhook": None,
        "token": None,
        "content": message,
        "avatar_url": member.avatar_url,
        "username": f"{member.name}{tag}",
    }

    if not webhook:
        webhook = await create_webhook(event)

    data["webhook"] = webhook
    data["token"] = webhook.token

    if event.message.referenced_message:
        data["embed"] = build_reply_embed(event.message.referenced_message, member)

    if event.message.attachments:
        attachments = []
        for attachment in event.message.attachments:
            attachments.append(await attachment.read())
        data["attachments"] = attachments
    await event.app.rest.execute_webhook(**data)
    await event.message.delete()


async def get_webhooks(
    event: hikari.GuildMessageCreateEvent,
) -> hikari.PartialWebhook | None:
    """Helper function to a webhook from the channel if it exists."""
    bot_username = os.environ.get("BOT_NAME")
    webhooks = await event.app.rest.fetch_guild_webhooks(event.guild_id)

    for webhook in webhooks:
        if webhook.channel_id == event.channel_id and f"{bot_username}-Proxy" == webhook.name:
            return webhook

    return None


async def create_webhook(
    event: hikari.GuildMessageCreateEvent,
) -> IncomingWebhook:
    """Helper function to create a webhook with the
    bots name."""
    bot_username = os.environ.get("BOT_NAME")
    webhook = await event.app.rest.create_webhook(
        channel=event.channel_id,
        name=f"{bot_username}-Proxy",
        reason="Webook for Patchwork Proxying!",
    )
    return webhook


def build_reply_embed(
    reply_message: hikari.Message | hikari.UndefinedType, member: PatchworkMember
) -> hikari.Embed | None:
    """Builds an embed to display a Reply message for proxying."""
    if isinstance(reply_message, hikari.UndefinedType):
        return None
    jump_url = reply_message.make_link(reply_message.guild_id)
    body = f"[**Replying to:**]({jump_url}) {reply_message.content}"
    embed = hikari.Embed(description=body)
    embed.set_author(
        name=f"{reply_message.author.username} {emoji.emojize(':right_arrow_curving_left:')}",
        icon=reply_message.author.avatar_url,
        url=jump_url,
    )
    if member is not None and member.color:
        embed.color = member.color

    return embed


@tanjun.as_loader
def load(client: tanjun.abc.Client) -> None:
    """Default tanjun loader"""
    client.add_component(component.copy())


@tanjun.as_unloader
def unload(client: tanjun.Client, /) -> None:
    client.remove_component_by_name(component.name)
