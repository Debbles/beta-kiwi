import aiopg
import aioredis
import tanjun
from models.front.system import SystemModel
from models.guild_settings import LocalGuildSettings
from tanjun import SlashContext

__all__ = [
    "has_registered_system",
    "has_local_guild_system_role",
]


async def has_registered_system(
    ctx: SlashContext,
    system_model: SystemModel = tanjun.injected(type=SystemModel),
    redis: aioredis.Redis = tanjun.injected(type=aioredis.Redis),
    pg_pool: aiopg.Pool = tanjun.injected(type=aiopg.Pool),
) -> bool:
    if not ctx.guild_id:
        await ctx.respond("This command must be used in a Server")
        return False
    system_model = await system_model.setup(ctx.member.id, pg_pool, redis)
    if not system_model:
        await ctx.respond("You must have a registered System.")
        return False
    return True


async def has_local_guild_system_role(
    ctx: SlashContext,
    redis: aioredis.Redis = tanjun.injected(type=aioredis.Redis),
    pg_pool: aiopg.Pool = tanjun.injected(type=aiopg.Pool),
) -> bool:
    if not ctx.guild_id or not ctx.member:
        await ctx.respond("This command must be used in a Server")
        return False
    local_guild_settings = await LocalGuildSettings.setup(ctx, redis, pg_pool)
    if not local_guild_settings:
        await ctx.respond("This server does not appear to have the settings for that!")
        return False
    system_role = await local_guild_settings.get_system_role(ctx)
    if not system_role:
        await ctx.respond("I can't find the role this Server is setup for... Please contact the Admins!")
        return False
    if system_role.id in ctx.member.role_ids:
        return True
    await ctx.respond(f"It doesn't look like you have the Proxy Role for this Server... {system_role.mention}.")
    return False
