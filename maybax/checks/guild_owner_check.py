from tanjun import SlashContext

__all__ = [
    "guild_owner_check",
]


async def guild_owner_check(ctx: SlashContext) -> bool:
    if not ctx.guild_id:
        return False
    guild = ctx.get_guild() if ctx.get_guild() else (await ctx.rest.fetch_guild(ctx.guild_id))
    if not guild:
        return False
    return guild.owner_id == ctx.author.id
