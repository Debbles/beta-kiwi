FROM python:3.9.5-slim

ENV PYTHONUNBUFFERED 1

RUN apt-get update -y && apt-get upgrade -y
RUN apt-get install build-essential git -y

RUN /usr/local/bin/python -m pip install --upgrade pip
RUN pip install --upgrade pip
RUN pip install poetry

RUN mkdir /app
WORKDIR /app/maybax

COPY ./pyproject.toml /pyproject.toml

RUN poetry config virtualenvs.create false
RUN poetry install 


CMD ["python", "run.py"]
