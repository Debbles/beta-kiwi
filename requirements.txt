aiofiles==0.7.0
aiohttp==3.8.1
aiopg==1.3.3
aioredis==2.0.0
aiosignal==1.2.0
anybadge==1.8.0
anyio==3.4.0
APScheduler==3.8.1
argcomplete==1.12.3
asgiref==3.4.1
astor==0.8.1
astroid==2.9.0
async-timeout==4.0.1
attrs==21.2.0
backports.entry-points-selectable==1.1.1
bandit==1.7.1
black==21.11b1
certifi==2021.10.8
charset-normalizer==2.0.8
click==8.0.3
codespell==2.1.0
colorlog==6.6.0
coverage==6.2
debugpy==1.5.1
distlib==0.3.3
emoji==1.6.1
filelock==3.4.0
flake8==4.0.1
flake8-bandit==2.1.2
flake8-black==0.2.3
flake8-deprecated==1.3
flake8-functions==0.0.6
flake8-if-statements==0.1.0
flake8-isort==4.1.1
flake8-mutable==1.2.0
flake8-pep3101==1.3.0
flake8-plugin-utils==1.3.2
flake8-polyfill==1.0.2
flake8-print==4.0.0
flake8-printf-formatting==1.1.2
flake8-pytest-style==1.5.1
frozenlist==1.2.0
gitdb==4.0.9
GitPython==3.1.24
h11==0.12.0
hikari==2.0.0.dev104
hikari-tanjun==2.2.0a1
hikari-yuyo==1.0.3a1
httpcore==0.13.7
httpx==0.19.0
idna==3.3
iniconfig==1.1.1
isort==5.10.1
lazy-object-proxy==1.6.0
mccabe==0.6.1
mr-proper==0.0.7
multidict==5.2.0
mypy-extensions==0.4.3
new-relic-logger-for-python==0.2.0
newrelic==6.10.0.165
nodeenv==1.6.0
nox==2021.10.1
packaging==21.3
pathspec==0.9.0
pbr==5.8.0
pip==21.3.1
platformdirs==2.4.0
pluggy==1.0.0
psutil==5.8.0
psycopg2-binary==2.9.2
pure25519==0.0.1
py==1.11.0
pycodestyle==2.8.0
pydantic==1.8.2
pyflakes==2.4.0
pylint==2.12.1
pylint-exit==1.2.0
pyparsing==3.0.6
pyright==0.0.10
pytest==6.2.5
pytest-asyncio==0.15.1
pytest-cov==2.12.1
pytest-mock==3.6.1
pytest-pyright==0.0.2
pytz==2021.3
pytz-deprecation-shim==0.1.0.post0
PyYAML==6.0
regex==2021.11.10
requests==2.26.0
rfc3986==1.5.0
setuptools==56.0.0
six==1.16.0
smmap==5.0.0
sniffio==1.2.0
stdlib-list==0.8.0
stevedore==3.5.0
testfixtures==6.18.3
toml==0.10.2
tomli==1.2.2
typing_extensions==4.0.0
tzdata==2021.5
tzlocal==4.1
urllib3==1.26.7
uvloop==0.16.0
virtualenv==20.10.0
vulture==2.3
wrapt==1.13.3
yarl==1.7.2
