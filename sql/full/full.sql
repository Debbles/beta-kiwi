BEGIN;
--
-- Create model GuildOwnerSettings
--
CREATE TABLE "guildownersettings_guildownersettings" ("id" bigserial NOT NULL PRIMARY KEY, "guild_id" bigint NOT NULL UNIQUE, "logging_channel" bigint NULL, "vc_logging_channel" bigint NULL, "mod_logging_channel" bigint NULL);
COMMIT;

BEGIN;
--
-- Create model Group
--
CREATE TABLE "system_group" ("id" bigserial NOT NULL PRIMARY KEY, "pluralkit_id" varchar(20) NULL UNIQUE, "tupper_id" varchar(20) NULL UNIQUE, "name" varchar(255) NULL, "description" text NULL, "tag" text NULL, "avatar_url" text NULL, "timezone" varchar(63) NULL, "created_at" timestamp with time zone NOT NULL);
--
-- Create model System
--
CREATE TABLE "system_system" ("id" bigserial NOT NULL PRIMARY KEY, "pluralkit_id" varchar(20) NULL UNIQUE, "tupper_id" varchar(20) NULL UNIQUE, "name" varchar(255) NULL, "description" text NULL, "tag" varchar(255) NULL, "avatar_url" text NULL, "timezone" varchar(63) NULL, "created_at" timestamp with time zone NOT NULL, "owner_discord_id" varchar(255) NULL UNIQUE);
--
-- Create model Member
--
CREATE TABLE "system_member" ("id" bigserial NOT NULL PRIMARY KEY, "pluralkit_id" varchar(20) NULL UNIQUE, "tupper_id" varchar(20) NULL UNIQUE, "name" varchar(255) NOT NULL, "tag" varchar(255) NULL, "indicators" jsonb NULL, "pronouns" text NULL, "color" varchar(6) NULL, "avatar_url" text NULL, "birthday" date NULL, "description" text NULL, "position" varchar(255) NULL, "display_name" varchar(255) NULL, "tupper_group_id" varchar(20) NULL, "tupper_group_position" varchar(20) NULL, "created_at" timestamp with time zone NOT NULL, "groups_id" bigint NULL, "system_id" bigint NOT NULL);
--
-- Add field system to group
--
ALTER TABLE "system_group" ADD COLUMN "system_id" bigint NOT NULL CONSTRAINT "system_group_system_id_38fbb4b7_fk_system_system_id" REFERENCES "system_system"("id") DEFERRABLE INITIALLY DEFERRED; SET CONSTRAINTS "system_group_system_id_38fbb4b7_fk_system_system_id" IMMEDIATE;
CREATE INDEX "system_group_pluralkit_id_a6238f31_like" ON "system_group" ("pluralkit_id" varchar_pattern_ops);
CREATE INDEX "system_group_tupper_id_b9132664_like" ON "system_group" ("tupper_id" varchar_pattern_ops);
CREATE INDEX "system_system_pluralkit_id_12efc2e1_like" ON "system_system" ("pluralkit_id" varchar_pattern_ops);
CREATE INDEX "system_system_tupper_id_d02d80f4_like" ON "system_system" ("tupper_id" varchar_pattern_ops);
CREATE INDEX "system_system_owner_discord_id_553a16e7_like" ON "system_system" ("owner_discord_id" varchar_pattern_ops);
ALTER TABLE "system_member" ADD CONSTRAINT "system_member_groups_id_55dcf897_fk_system_group_id" FOREIGN KEY ("groups_id") REFERENCES "system_group" ("id") DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "system_member" ADD CONSTRAINT "system_member_system_id_d88ede5b_fk_system_system_id" FOREIGN KEY ("system_id") REFERENCES "system_system" ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE INDEX "system_member_pluralkit_id_85c7fe3f_like" ON "system_member" ("pluralkit_id" varchar_pattern_ops);
CREATE INDEX "system_member_tupper_id_b4dea94e_like" ON "system_member" ("tupper_id" varchar_pattern_ops);
CREATE INDEX "system_member_groups_id_55dcf897" ON "system_member" ("groups_id");
CREATE INDEX "system_member_system_id_d88ede5b" ON "system_member" ("system_id");
CREATE INDEX "system_group_system_id_38fbb4b7" ON "system_group" ("system_id");
COMMIT;


BEGIN;
--
-- Create model GuildSystemSettings
--
CREATE TABLE "system_guildsystemsettings" ("id" bigserial NOT NULL PRIMARY KEY, "system_role" bigint NOT NULL, "guild_id" bigint NOT NULL UNIQUE);
ALTER TABLE "system_guildsystemsettings" ADD CONSTRAINT "system_guildsystemse_guild_id_54a0041f_fk_guildowne" FOREIGN KEY ("guild_id") REFERENCES "guildownersettings_guildownersettings" ("id") DEFERRABLE INITIALLY DEFERRED;
COMMIT;
BEGIN;
--
-- Create model AutoRoles
--
CREATE TABLE "autoroles_autoroles" ("id" bigserial NOT NULL PRIMARY KEY, "emoji" varchar(10) NOT NULL, "role_id" bigint NOT NULL, "role_type" varchar(10) NOT NULL, "guild_id" bigint NOT NULL UNIQUE);
ALTER TABLE "autoroles_autoroles" ADD CONSTRAINT "autoroles_autoroles_guild_id_97f5f3aa_fk_guildowne" FOREIGN KEY ("guild_id") REFERENCES "guildownersettings_guildownersettings" ("id") DEFERRABLE INITIALLY DEFERRED;
COMMIT;

